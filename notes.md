Asm Source vs Machine Code
- Source has labels, references and expressions
- Source has instructions with unknown addressing mode (which means unknown byte & cycle count)

Asm Fragments vs Small Assembler
- Assembler must find specific addressing mode, sizes and addresses,
- Good assembler must support macros for data and code, images, tables, sounds, disks, ... 
- Good assembler must support linking


A General Solver
- Labels and Consts are all in a "global scope" and can depend on each other.
- Sizes of (some) instructions depend on the addressing mode of the instruction.
- The formula for each of these is added to a huge constraint pool.
- The solver solves the formulas in the pool.

Features for Asm6502
- Support for (all?) 6502 CPU's - including all the special addressing modes and illegal instructions.
- Classic 6502 ASM syntax for instructions and addressing modes
- Labels (for jumping, branching, naming tables and variables in memory etc.)
- Expressions / Calculations

- Blocks (with scopes)

Macro Data features
- Include Binary files
- Include SID's
- Images (conversion to proper memory layout)
- Pre-computed tables. Sines, Low/High values, Absolute Values, Easing, Multiplication, Perspective, ...)

Macro Code Features
- Unrolled code
- Conditional code

Linking & Packaging, Memory Management
- Segments and linking instructions (or similar)
- Crunchers
- Disk Formats (D64, ...) (pluggable)
- Pseudo PC (putting binary generated code in another place than where it will be executed)
- Loaders?

- Support Memory Banking?

- Automatically Determine Addressing Mode (Absolute or Zero-Page)

- Functions (for expressions)

Nice-to-have
- Adding new CPU's through a plug-in or by configuration? (Addressing modes, Instructions)
- Automatically convert long branches 

