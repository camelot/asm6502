package dk.camelot64.asm6502.fragment.model;

import java.util.Objects;

/** A specific rewrite that can be performed on a model. This is an instantiated rewrite rule. */
public sealed interface SpecificRewrite {

  /**
   * Get the rewrite rule.
   *
   * @return The rule
   */
  Rewrite rule();

  /**
   * Perform the specific rewrite producing a new model.
   *
   * @param model The model to rewrite from
   * @param nextId The ID that will be assigned to the new model.
   * @return The new rewritten model
   */
  Model rewrite(Model model, int nextId);

  public record RewriteModel(Rewrite.ModelRule rule) implements SpecificRewrite {

    public Model rewrite(Model model, int newModelId) {
      return rule.rewrite(model, newModelId);
    }

    public String toString() {
      return rule.getClass().getSimpleName();
    }
  }

  /**
   * A rewrite rule instantiated for a specific operation in the model.
   *
   * @param operation The operation to rewrite
   * @param rule The rewrite rule
   */
  public record RewriteOperation(Model.Operation operation, Rewrite.OperationRule rule)
      implements SpecificRewrite {

    @Override
    public Model rewrite(Model model, int newModelId) {
      return rule.rewrite(operation, model, newModelId);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      RewriteOperation that = (RewriteOperation) o;
      return operation.inputs().equals(that.operation.inputs())
          && operation.outputs().equals(that.operation.outputs())
          && rule.equals(that.rule);
    }

    @Override
    public int hashCode() {
      return Objects.hash(operation.inputs(), operation.outputs(), rule);
    }

    @Override
    public String toString() {
      return rule.getClass().getSimpleName() + "@" + operation();
    }
  }
}
