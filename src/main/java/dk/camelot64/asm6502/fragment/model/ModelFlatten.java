package dk.camelot64.asm6502.fragment.model;

import dk.camelot64.asm6502.fragment.Fragment;
import java.util.*;
import java.util.function.Function;

/** Flatten an ASM {@link Model} DAG to a sequence. Ensure the operation sequence is sound. */
public class ModelFlatten {

  /**
   * Find a valid flat sequence to execute the operations of a fragment operation model. The flat
   * sequence ensures that operations that produce output data that is input to another operation is
   * executed before the other operation.
   *
   * @param model The ASM model to linearize
   * @return The flat sequence of the operations.
   */
  public static List<Fragment.Instruction> flatten(Model model) {
    FlattenState initialState = initialState(model);
    final List<FlattenState> sequences = findSequences(initialState, model);
    return findShortestSequence(sequences);
  }

  /**
   * Find the shortest sequence.
   *
   * @param sequences The sequences to choose from.
   * @return The shortest sequence
   */
  private static List<Fragment.Instruction> findShortestSequence(List<FlattenState> sequences) {
    FlattenState shortest = null;
    int minLength = Integer.MAX_VALUE;
    for (FlattenState sequence : sequences) {
      if (sequence.instructions.size() < minLength) {
        minLength = sequence.instructions.size();
        shortest = sequence;
      }
    }
    return shortest.instructions();
  }

  /**
   * State of a place used during model flattening.
   *
   * @param remainingUses The number of remaining usages of the place, i.e. the number of unexecuted
   *     instructions that use the place as input. Zero when the place is no longer needed.
   * @param ready true if the place is ready to use (meaning the instruction generating it has been
   *     executed.) and needed by an {@link Model.Operation} that has not yet been executed.
   * @param binding If the place is currently in a register, this is the register holding it. (Null
   *     if the place is not in a register)
   * @param tmpStorage If the place is currently in temporary storage this is the identifier of the
   *     storage. Places are placed in temporary storage if another instruction would otherwise
   *     clobber the binding. (Null if the place is not in temporary storage)
   */
  private record PlaceState(
      int remainingUses,
      boolean ready,
      Model.Place.Reg binding,
      Fragment.Expr.Identifier tmpStorage) {

    public PlaceState() {
      this(0, false, null, null);
    }

    /**
     * Create a new place state with an updated register binding.
     *
     * @param reg The register to bind the place to
     * @return The new place state
     */
    public PlaceState withBinding(Model.Place.Reg reg) {
      return new PlaceState(remainingUses, ready, reg, tmpStorage);
    }

    public PlaceState withRemainingUsesIncrease() {
      return new PlaceState(remainingUses + 1, ready, binding, tmpStorage);
    }

    public PlaceState withRemainingUsesDecrease() {
      return new PlaceState(remainingUses - 1, ready, binding, tmpStorage);
    }

    public PlaceState withReady(boolean ready) {
      return new PlaceState(remainingUses, ready, binding, tmpStorage);
    }

    public PlaceState withTmpStorage(Fragment.Expr.Identifier tmpStorage) {
      return new PlaceState(remainingUses, ready, binding, tmpStorage);
    }
  }

  /**
   * State used to flatten the model to legal ASM sequences.
   *
   * @param placeStates The status of the places of the model.
   * @param executed The operations that have already been executed.
   * @param instructions The sequence of instructions generated. This is a mix of ASM operations and
   *     save/load to temporary storage.
   */
  private record FlattenState(
      Map<Model.Place, PlaceState> placeStates,
      List<Model.Operation> executed,
      List<Fragment.Instruction> instructions) {

    private FlattenState {
      Objects.requireNonNull(placeStates);
      Objects.requireNonNull(executed);
      Objects.requireNonNull(instructions);
    }

    FlattenState withRemainingUsesIncrease(Model.Place place) {
      return withUpdate(place, PlaceState::withRemainingUsesIncrease);
    }

    FlattenState withRemainingUsesDecrease(Model.Place place) {
      return withUpdate(place, PlaceState::withRemainingUsesDecrease);
    }

    public boolean hasRemainingUses(Model.Place place) {
      return placeStates.get(place).remainingUses() > 0;
    }

    public FlattenState withReady(Model.Place place, boolean ready) {
      return withUpdate(place, placeState -> placeState.withReady(ready));
    }

    public boolean isReady(List<Model.Place> places) {
      return places.stream().map(placeStates::get).allMatch(PlaceState::ready);
    }

    public FlattenState withBinding(Model.Place place, Model.Place.Reg reg) {
      if (reg != null && getBinding(reg) != null) {
        throw new RuntimeException("Multiple bindings for a single register not allowed!");
      }
      return withUpdate(place, placeState -> placeState.withBinding(reg));
    }

    public Model.Place.Register getBinding(Model.Place.Reg reg) {
      for (Model.Place place : placeStates.keySet()) {
        if (reg.equals(placeStates.get(place).binding())) {
          return (Model.Place.Register) place;
        }
      }
      return null;
    }

    public FlattenState withTmpStorage(Model.Place place, Fragment.Expr.Identifier tmpStorage) {
      return withUpdate(place, placeState -> placeState.withTmpStorage(tmpStorage));
    }

    public Fragment.Expr.Identifier getTmpStorage(Model.Place place) {
      return placeStates.get(place).tmpStorage;
    }

    public boolean hasTmpStorage(Fragment.Expr.Identifier tmpIdentifier) {
      return placeStates.values().stream()
          .anyMatch(placeState -> tmpIdentifier.equals(placeState.tmpStorage));
    }

    public FlattenState withExecuted(Model.Operation asmOperation) {
      return new FlattenState(placeStates, withAppend(executed, asmOperation), instructions);
    }

    public boolean isExecuted(Model.Operation instruction) {
      return executed.contains(instruction);
    }

    public FlattenState withInstruction(Fragment.Instruction instruction) {
      if (instruction.equals(Fragment.Instruction.NONE)) {
        return this;
      } else {
        return new FlattenState(placeStates, executed, withAppend(instructions, instruction));
      }
    }

    private FlattenState withUpdate(Model.Place place, Function<PlaceState, PlaceState> update) {
      final HashMap<Model.Place, PlaceState> updatedPlaceStates = new HashMap<>(placeStates);
      updatedPlaceStates.put(place, update.apply(placeStates.get(place)));
      return new FlattenState(
          Collections.unmodifiableMap(updatedPlaceStates), executed, instructions);
    }

    public static <T> List<T> withAppend(List<T> list, T element) {
      final ArrayList<T> newList = new ArrayList<>(list);
      newList.add(element);
      return Collections.unmodifiableList(newList);
    }
  }

  private static FlattenState initialState(Model model) {

    // Create initial place states
    Map<Model.Place, PlaceState> placeStates = new HashMap<>();
    for (Model.Place place : model.places()) {
      placeStates.put(place, new PlaceState());
    }
    // Count unused places
    FlattenState state = new FlattenState(placeStates, List.of(), List.of());
    for (Model.Operation instruction : model.operations()) {
      for (Model.Place input : instruction.inputs()) {
        state = state.withRemainingUsesIncrease(input);
      }
    }
    // Find ready places
    List<Model.Place> readyPlaces = new ArrayList<>(model.places());
    for (Model.Operation instruction : model.operations()) {
      readyPlaces.removeAll(instruction.outputs());
    }
    // Update ready places and initial bindings
    for (Model.Place readyPlace : readyPlaces) {
      state = state.withReady(readyPlace, true);
      if (readyPlace instanceof Model.Place.Register registerPlace) {
        state = state.withBinding(readyPlace, registerPlace.reg());
      }
    }
    return state;
  }

  /**
   * Enumerate all full sequences from an existing partial sequence. This method calls itself
   * recursively.
   *
   * @param prevState The current flatten state of the partial sequence.
   * @param model The instruction model describing the legal sequences.
   * @return All full sequences extending the partial sequence.
   */
  static List<FlattenState> findSequences(FlattenState prevState, Model model) {
    // Find next instructions
    List<Model.Operation> readyInstructions = new ArrayList<>();
    for (Model.Operation instruction : model.operations()) {
      if (!prevState.isExecuted(instruction) && prevState.isReady(instruction.inputs())) {
        readyInstructions.add(instruction);
      }
    }
    // Execute each instruction
    List<FlattenState> finalSequences = new ArrayList<>();
    for (Model.Operation instruction : readyInstructions) {
      List<FlattenState> sequences = findSequences(prevState, instruction, model);
      finalSequences.addAll(sequences);
    }
    return finalSequences;
  }

  /**
   * Find all sequences from a partial sequence that result from executing a specific next
   * instruction.
   *
   * @param prevState The current flatten state of the partial sequence.
   * @param instruction The instruction that was chosen for execution.
   * @param model The instruction model describing the legal sequences.
   * @return All full sequences extending the partial sequence.
   */
  private static List<FlattenState> findSequences(
      FlattenState prevState, Model.Operation instruction, Model model) {
    // Copy the previous state
    FlattenState nextState = prevState;

    // Update unused counts for inputs
    for (Model.Place input : instruction.inputs()) {
      nextState = nextState.withRemainingUsesDecrease(input);
    }

    // 1. Load inputs from temporary storage
    for (Model.Place input : instruction.inputs()) {
      if (input instanceof Model.Place.Register inputRegister) {
        // Is the register already correctly bound
        final Model.Place.Reg inputRegisterBinding = nextState.placeStates.get(input).binding();
        if (inputRegister.reg().equals(inputRegisterBinding)) {
          continue;
        }
        // Should we save the current register binding to tmp storage before loading.
        final Model.Place.Register currentBinding = nextState.getBinding(inputRegister.reg());
        if (currentBinding != null && nextState.hasRemainingUses(currentBinding)) {
          // TODO: Only save if it is clobbered?
          nextState = saveToTempStorage(nextState, currentBinding);
        }
        // Load the input register from tmp storage
        nextState = loadFromTempStorage(nextState, inputRegister);
      }
    }

    // 2. Does the instruction clobber an unused binding?
    for (Model.Place output : instruction.outputs()) {
      if (output instanceof Model.Place.Register outputRegister) {
        final Model.Place.Register currentBinding = nextState.getBinding(outputRegister.reg());
        if (currentBinding != null
            && !currentBinding.equals(outputRegister)
            && nextState.hasRemainingUses(currentBinding)) {
          // Yes: Save the binding to temporary storage.
          nextState = saveToTempStorage(nextState, currentBinding);
        }
      }
    }

    // 3. Create a new partial sequence with the added instruction. (Update the flatten state)
    nextState = nextState.withExecuted(instruction);
    nextState = nextState.withInstruction(instruction.asm());
    // Remove any ready places that are used up (unused count is zero)
    for (Model.Place input : instruction.inputs()) {
      if (!nextState.hasRemainingUses(input)) {
        // Last usage - remove from ready places
        nextState = nextState.withReady(input, false);
        // And remove from bindings (if it was bound)
        nextState = nextState.withBinding(input, null);
      }
    }
    // Update bindings and add new ready places
    for (Model.Place output : instruction.outputs()) {
      if (output instanceof Model.Place.Register outputRegister) {
        nextState = nextState.withBinding(outputRegister, outputRegister.reg());
      }
      // New ready place
      nextState = nextState.withReady(output, true);
    }

    // 4. If the new sequence is still partial apply recursion.
    if (nextState.executed.size() < model.operations().size()) {
      return findSequences(nextState, model);
    } else {
      return List.of(nextState);
    }
  }

  private static FlattenState loadFromTempStorage(
      FlattenState nextState, Model.Place.Register inputRegister) {
    final Fragment.Expr.Identifier tmpIdentifier = nextState.getTmpStorage(inputRegister);
    if (tmpIdentifier == null) {
      throw new RuntimeException("Place not found in temporary storage " + inputRegister);
    }
    final String mnemonic = inputRegister.reg().loadMnemonic();
    nextState =
        nextState.withInstruction(
            new Fragment.Instruction(mnemonic, Fragment.AddressingMode.ABS, tmpIdentifier));
    nextState = nextState.withBinding(inputRegister, inputRegister.reg());
    // TODO: If we need the temporary storage again then do not clear it
    nextState = nextState.withTmpStorage(inputRegister, null);
    return nextState;
  }

  private static FlattenState saveToTempStorage(
      FlattenState nextState, Model.Place.Register currentBinding) {
    Fragment.Expr.Identifier tmpIdentifier;
    for (int i = 0; ; i++) {
      final String tmpStorageId = "tmp" + i;
      tmpIdentifier = new Fragment.Expr.Identifier(tmpStorageId);
      if (!nextState.hasTmpStorage(tmpIdentifier)) break;
    }
    final String mnemonic = currentBinding.reg().storeMnemonic();
    nextState =
        nextState.withInstruction(
            new Fragment.Instruction(mnemonic, Fragment.AddressingMode.ABS, tmpIdentifier));
    nextState = nextState.withTmpStorage(currentBinding, tmpIdentifier);
    nextState = nextState.withBinding(currentBinding, null);
    return nextState;
  }
}
