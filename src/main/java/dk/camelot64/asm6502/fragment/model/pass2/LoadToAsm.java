package dk.camelot64.asm6502.fragment.model.pass2;

import dk.camelot64.asm6502.fragment.*;
import dk.camelot64.asm6502.fragment.model.InstructionBuilder;
import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;
import java.util.List;

/** Rewrite MOV(U8, REG) to LDA/LDX/LDY ASM. */
public class LoadToAsm implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return !operation.isAsm()
        && operation.isLoad()
        && (operation.input().isAnyMem() || operation.input().isConst());
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final Model.Place.Register register = (Model.Place.Register) operation.output();
    String mnemonic = register.reg().loadMnemonic();
    final Fragment.Instruction asm = InstructionBuilder.instruction(mnemonic, operation.input());
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    final List<Model.Place> inputs = model.getEffectiveInputs(operation);
    builder.addAsmOperation(asm, operation, inputs);
    builder.remove(operation);
    return builder.model();
  }
}
