package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Initial identity rewrite used start the rewrite algorithm. */
public class Initial implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return true;
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    ModelBuilder builder = new ModelBuilder(newModelId, model);
    return builder.model();
  }
}
