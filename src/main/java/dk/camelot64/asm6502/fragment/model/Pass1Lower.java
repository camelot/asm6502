package dk.camelot64.asm6502.fragment.model;

import dk.camelot64.asm6502.fragment.Forger;
import dk.camelot64.asm6502.fragment.ForgerDebug;
import dk.camelot64.asm6502.fragment.model.Model.Operation;
import dk.camelot64.asm6502.fragment.model.pass1.*;
import java.util.*;

/**
 * Rewrites a {@link Model} to make it convertible to ASM. Each rewrite lowers the abstraction level
 * making the model closer to ASM.
 */
public class Pass1Lower {

  /**
   * A stored state of a model rewrite in progress. Used for keeping a task-list of multiple
   * rewrites to explore.
   *
   * @param model The current model
   * @param specificRewrite The next rewrite that should be performed on the model.
   */
  record ModelRewriteState(Model model, SpecificRewrite specificRewrite) {
    public Model rewrite(Forger.IdAllocator idAllocator) {
      return specificRewrite.rewrite(model, idAllocator.nextId());
    }
  }

  /**
   * Rewrite a model to make it convertible to ASM by lowering the abstraction level. The rewrite
   * will explore some alternatives which may produce better ASM.
   *
   * @param initialModel The model to rewrite
   * @param idAllocator ID-allocator for model IDs.
   * @param debug Debugger that will receive all intermediate models.
   * @return A list of rewritten lowered models. Each returned model is an alternative which may
   *     produce different ASM with the same semantics.
   */
  public static List<Model> rewrite(
      Model initialModel, Forger.IdAllocator idAllocator, ForgerDebug debug) {
    // Start the task-list with an identity rewrite
    final List<ModelRewriteState> rewriteTaskList = new ArrayList<>();
    final SpecificRewrite identity =
        new SpecificRewrite.RewriteOperation(initialModel.operations().get(0), new Initial());
    rewriteTaskList.add(new ModelRewriteState(initialModel, identity));
    List<Model> finalModels = new ArrayList<>();
    do {
      // Execute the next rewrite
      final ModelRewriteState rewriteState = rewriteTaskList.remove(0);
      Model newModel = rewriteState.rewrite(idAllocator);
      final Model model = rewriteState.model;
      debug.model(
          newModel,
          rewriteState.specificRewrite.rule().getClass().getSimpleName() + " (" + model.id() + ")",
          model);
      // Check that the model is still healthy
      ModelInvariants.assertModel(newModel, rewriteState.specificRewrite);
      // Find the next rewrite to perform
      final List<SpecificRewrite> nextRewrites = getNextRewrites(newModel);
      if (nextRewrites.size() == 0) {
        // A final model - return it.
        finalModels.add(newModel);
      } else {
        // More rewrites are required - add alternatives to the task-list
        ListIterator<SpecificRewrite> it = nextRewrites.listIterator(nextRewrites.size());
        while (it.hasPrevious()) {
          SpecificRewrite rewrite = it.previous();
          rewriteTaskList.add(0, new ModelRewriteState(newModel, rewrite));
        }
      }
      if (newModel.id() > 100) {
        throw new RuntimeException("Too many rounds. Potential infinite loop!");
      }
    } while (rewriteTaskList.size() > 0);
    return finalModels;
  }

  /**
   * Get the next lowering rewrites to run for a specific model.
   *
   * @param model The model to consider.
   * @return The next rewrite to run. Returns multiple rewrites if multiple matching rewrite
   *     alternatives are encountered. Returns an empty list if no rules are applicable.
   */
  private static List<SpecificRewrite> getNextRewrites(Model model) {
    // Look through operation rewrites
    for (Operation operation : model.operations()) {
      for (Rewrite rewrite : REWRITES) {
        if (rewrite instanceof Rewrite.OperationRule rule) {
          if (rule.match(operation, model)) {
            return List.of(new SpecificRewrite.RewriteOperation(operation, rule));
          }
        } else if (rewrite instanceof Rewrite.Alternatives alternatives) {
          List<SpecificRewrite> matches = new ArrayList<>();
          for (Rewrite.OperationRule rule : alternatives.rewrites()) {
            if (rule.match(operation, model)) {
              matches.add(new SpecificRewrite.RewriteOperation(operation, rule));
            }
          }
          if (matches.size() > 0) {
            return matches;
          }
        }
      }
    }
    // Look through model rewrites
    for (Rewrite lowerRewrite : REWRITES) {
      if (lowerRewrite instanceof Rewrite.ModelRule modelRule) {
        if (modelRule.match(model)) {
          return List.of(new SpecificRewrite.RewriteModel(modelRule));
        }
      }
    }
    return List.of();
  }

  /** The rewrites used for lowering the model. */
  static final List<Rewrite> REWRITES =
      List.of(
          new U16ByU8(),
          new DerefIdxMemThroughYy(),
          new Rewrite.Alternatives(
              List.of(new DerefIdxConstThroughXx(), new DerefIdxConstThroughYy())),
          new DerefConstToMemory(),
          new CollapseEquivalentSiblings(),
          new DerefIdxToZpy(),
          new DerefIdxToAbsXy(),
          new CollapseIdentityMov(),
          new CollapseMovAaToAny(),
          new MovToZpyThroughAa(),
          new MovFromZpyThroughAa(),
          new MovToAbsXyThroughAa(),
          new MovNonRegThroughAa(),
          new Rewrite.Alternatives(
              List.of(new BinInputLeftThroughAa(), new BinInputRightThroughAa())),
          new BinOutputThroughAa(),
          new BinSimpleSwapAaLeft());
}
