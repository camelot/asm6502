package dk.camelot64.asm6502.fragment.model;

import java.util.List;

/** Rewrites a {@link Model} lowering the abstraction level, making it closer to ASM. */
public interface Rewrite {

  /**
   * A rule that can rewrite a model operation (and its surrounding places/operations) lowering the
   * abstraction level. The rule can only rewrite model operations that match certain criteria.
   */
  interface OperationRule extends Rewrite {
    /**
     * Determine if the rule can successfully rewrite a specific model operation.
     *
     * @param operation The operation to examine.
     * @param model The model.
     * @return true if the model can rewrite the model operation.
     */
    boolean match(Model.Operation operation, Model model);

    /**
     * Rewrite a model operation to lower the abstraction level. Can assume that {@link
     * #match(Model.Operation, Model)} returned true.
     *
     * @param operation The operation to rewrite.
     * @param model The model to rewrite. This model is not modified.
     * @param newModelId The ID to assign to the new model
     * @return A new model, which is a rewritten version of the passed model.
     */
    Model rewrite(Model.Operation operation, Model model, int newModelId);
  }

  /**
   * A rule that can rewrite an entire model lowering the abstraction level. The rule can only
   * rewrite models that match certain criteria.
   */
  interface ModelRule extends Rewrite {
    /**
     * Determine if the rule can successfully rewrite a specific model.
     *
     * @param model The model.
     * @return true if the model can rewrite the model.
     */
    boolean match(Model model);

    /**
     * Rewrite a model to lower the abstraction level. Can assume that {@link #match(Model)}
     * returned true.
     *
     * @param model The model to rewrite. This model is not modified.
     * @param newModelId The ID to assign to the new model
     * @return A new model, which is a rewritten version of the passed model.
     */
    Model rewrite(Model model, int newModelId);
  }

  /**
   * Alternative rewrites for a model that produce different results, which may produce different
   * ASM. The rewriting algorithm will test the different alternatives to determine which produces
   * the best ASM.
   */
  record Alternatives(List<OperationRule> rewrites) implements Rewrite {}
}
