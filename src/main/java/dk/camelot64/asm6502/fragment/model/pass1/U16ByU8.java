package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Rewrite all 16-bit operations to use two 8-bit operations. */
public class U16ByU8 implements Rewrite.ModelRule {

  @Override
  public boolean match(Model model) {
    return model.operations().stream().anyMatch(this::is16bit);
  }

  private boolean is16bit(Model.Operation operation) {
    return (operation.isMov() || operation.isBinSimple())
        && operation.inputs().stream().allMatch(Model.Place::isU16)
        && operation.output().isU16()
        && (operation.inputs().stream()
            .allMatch(place -> place.isConst() || place.isAnyMem() || place.isAny()))
        && (operation.output().isAnyMem() || operation.output().isAny());
  }

  /** Lo/hi-bytes for a 16bit place. */
  record Bytes(Model.Place loPlace, Model.Place hiPlace) {}

  @Override
  public Model rewrite(Model model, int newModelId) {
    final ModelBuilder builder = new ModelBuilder(newModelId, model);

    // Create two 8bit places for each 16bit places
    Map<Model.Place, Bytes> bytePlaces = new HashMap<>();
    for (Model.Place place : model.places()) {
      if (place.isU16()) {
        bytePlaces.put(
            place,
            new Bytes(
                builder.addBytePlace(place, place.id().lValue(), 0),
                builder.addBytePlace(place, place.id().lValue(), 1)));
      }
    }

    // Convert all 16bit operations to two 8bit-operations
    for (Model.Operation operation : model.operations()) {
      if (is16bit(operation)) {
        final Model.Place outLoPlace = bytePlaces.get(operation.output()).loPlace;
        final Model.Place outHiPlace = bytePlaces.get(operation.output()).hiPlace;
        List<Model.Place> inLoPlaces = new ArrayList<>();
        List<Model.Place> inHiPlaces = new ArrayList<>();
        for (Model.Place input : operation.inputs()) {
          inLoPlaces.add(bytePlaces.get(input).loPlace);
          inHiPlaces.add(bytePlaces.get(input).hiPlace);
        }
        // Add lo/hi 8bit operations
        builder.addOperation(operation.op(), List.of(outLoPlace), inLoPlaces);
        builder.addOperation(operation.op(), List.of(outHiPlace), inHiPlaces);
        builder.remove(operation);
      }
    }

    // Remove places that are no longer used
    builder.removeUnusedPlaces();

    return builder.model();
  }
}
