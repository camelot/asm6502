package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Rewrite MOV(U8, U8) where none are registers to use intermediate MOV(AA, U8), MOV(U8, AA). */
public class MovNonRegThroughAa implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isMov()
        && operation.input().isU8()
        && operation.output().isU8()
        && !operation.input().isReg()
        && !operation.input().isAny()
        && !operation.output().isReg()
        && !operation.output().isAny();
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    final Model.Place.Register aaPlace = builder.addPlaceAa(operation.input().type());
    builder.addOperation(Model.Op.MOV, aaPlace, operation.input());
    builder.addOperation(Model.Op.MOV, operation.output(), aaPlace);
    builder.remove(operation);
    return builder.model();
  }
}
