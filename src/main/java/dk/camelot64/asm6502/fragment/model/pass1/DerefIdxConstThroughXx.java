package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/**
 * Rewrite DEREF_IDX(CONST, MEM/const) to use intermediate XX MOV(XX, MEM/const), DEREF_IDX(ZP, XX).
 */
public class DerefIdxConstThroughXx implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isDerefIdx()
        && operation.inputLeft().isConst()
        && (operation.inputRight().isAnyMem() || operation.inputRight().isConst());
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    final Model.Place.Register xxPlace = builder.addPlaceXx(operation.inputRight().type());
    builder.addOperation(Model.Op.MOV, xxPlace, operation.inputRight());
    builder.addOperation(Model.Op.DEREF_IDX, operation.output(), operation.inputLeft(), xxPlace);
    builder.remove(operation);
    return builder.model();
  }
}
