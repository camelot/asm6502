package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Collapse MOV(AA,ANY) to just use AA. */
public class CollapseMovAaToAny implements Rewrite.OperationRule {

  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isMov() && operation.input().isRegAa() && operation.output().isAny();
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    ModelBuilder builder = new ModelBuilder(newModelId, model);
    builder.replace(operation.output(), operation.input());
    builder.remove(operation);
    return builder.model();
  }
}
