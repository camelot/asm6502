package dk.camelot64.asm6502.fragment.model;

import dk.camelot64.asm6502.fragment.Fragment;
import java.util.*;
import java.util.function.Predicate;

/**
 * A read-only model of the exact operation that an ASM fragment must perform. The model is
 * rewritten until it can be turned into a set of legal ASM instructions that execute the operation.
 * Models are created using {@link ModelBuilder}.
 */
public record Model(int id, List<Place> places, List<Operation> operations) {

  public Model {
    Objects.requireNonNull(places);
    Objects.requireNonNull(operations);
  }

  /**
   * Determine if the model has only ASM operations.
   *
   * @return true if all operations are ASM operations.
   */
  public boolean isAsm() {
    return operations().stream().allMatch(Operation::isAsm);
  }

  public Place get(PlaceId id) {
    return places.stream().filter(place -> place.id().equals(id)).findFirst().orElse(null);
  }

  public Operation findOnly(Predicate<Operation> predicate) {
    return operations().stream()
        .filter(predicate)
        .reduce(
            (op1, op2) -> {
              throw new RuntimeException("Multiple matching operations " + op1 + " " + op2);
            })
        .orElseThrow();
  }

  public Operation findOnlyOrNull(Predicate<Operation> predicate) {
    return operations().stream()
        .filter(predicate)
        .reduce(
            (op1, op2) -> {
              throw new RuntimeException("Multiple matching operations " + op1 + " " + op2);
            })
        .orElse(null);
  }

  public List<Operation> find(Predicate<Operation> predicate) {
    return operations().stream().filter(predicate).toList();
  }

  public List<Operation> operationsWithInput(Place place) {
    return find(operation -> operation.inputs().contains(place));
  }

  public Operation operationWithOutput(Place place) {
    return findOnly(op -> op.hasOutput(place));
  }

  public List<Operation> operationsWithOutput(Place place) {
    return find(op -> op.hasOutput(place));
  }

  /**
   * Get all effective inputs for an operation. The effective inputs are the actual inputs plus the
   * address & index of any abs_xy/ind_zpy input/output.
   *
   * @param operation The operation to examine
   * @return The effective inputs.
   */
  public List<Place> getEffectiveInputs(Operation operation) {
    final ArrayList<Place> effectiveInputs = new ArrayList<>(operation.inputs());
    effectiveInputs.addAll(operation.getReferencedPlaces().stream().map(this::get).toList());
    return effectiveInputs;
  }

  /**
   * Type of values (constant, in register, in flag or in memory) that is part of the ASM operation
   * model.
   */
  public sealed interface Type permits Type.Basic, Type.Pointer {

    enum Basic implements Type {
      FLAG,
      I8,
      U8,
      I16,
      U16
    }

    record Pointer(Model.Type toType) implements Type {

      public static Type to(Type toType) {
        return new Pointer(toType);
      }
    }
  }

  public enum PlaceKind {
    ZEROPAGE('z'),
    MAINMEMORY('m'),
    CONSTANT('c'),
    REGISTER('r'),
    OTHER('o');

    PlaceKind(char prefix) {
      this.prefix = prefix;
    }

    private final char prefix;

    public final char prefix() {
      return prefix;
    }
  }

  /** Identifies a place in the model. */
  public record PlaceId(PlaceKind kind, int idx, boolean lValue) {
    public static PlaceId of(PlaceKind kind, int id, boolean lValue) {
      return new PlaceId(kind, id, lValue);
    }

    @Override
    public String toString() {
      return name();
    }

    public String name() {
      return "" + kind.prefix() + idx + (lValue ? "l" : "");
    }

    public String replaceName() {
      return "" + kind.prefix() + idx;
    }
  }

  /**
   * The expression for computing the place address/value. For most places the expression is the ID
   * of the place itself.
   */
  public sealed interface PlaceExpr permits PlaceExpr.Id, PlaceExpr.ByteOf {

    public PlaceId placeId();

    public static Id id(PlaceId placeId) {
      return new Id(placeId);
    }

    public static ByteOf byteOf(PlaceId placeId, int byteOffset) {
      return new ByteOf(placeId, byteOffset);
    }

    /**
     * The address/value of the place is another place.
     *
     * @param placeId The ID of the place that holds the address/value of the place.
     */
    record Id(PlaceId placeId) implements PlaceExpr {}

    /**
     * The address/value of the place is a byte-offset from another place.
     *
     * @param placeId The ID of the place that holds the address/value of the place.
     * @param offset The byte offset from the other place.
     */
    record ByteOf(PlaceId placeId, int offset) implements PlaceExpr {}
  }

  /** A place that holds data. (register, flag, constant or memory). */
  public sealed interface Place
      permits Place.Any,
          Place.Constant,
          Place.Deref,
          Place.DerefIdx,
          Place.MemPlace,
          Place.Register {
    /**
     * Unique identification of the place within the model.
     *
     * @return The ID of the place.
     */
    PlaceId id();

    /**
     * Type of the place.
     *
     * @return The type
     */
    Type type();

    /**
     * Get any referenced places. This is any places referenced by a place. This does not include
     * the place itself.
     *
     * @return All referenced places.
     */
    default List<PlaceId> getReferenced() {
      final ArrayList<PlaceId> referenced = new ArrayList<>();
      if (this instanceof Constant constant) {
        addReferenced(referenced, constant.valueExpr());
      } else if (this instanceof MemPlace memPlace) {
        addReferenced(referenced, memPlace.addrExpr());
        if (this instanceof AbsXy absXy) {
          referenced.add(absXy.idxPlaceId());
        } else if (this instanceof IndirectZpY zpY) {
          referenced.add(zpY.yyPlaceId());
        }
      }
      return referenced;
    }

    private void addReferenced(ArrayList<PlaceId> referenced, PlaceExpr valueExpr) {
      final PlaceId valueExprPlaceId = valueExpr.placeId();
      if (!valueExprPlaceId.equals(this.id())) {
        referenced.add(valueExprPlaceId);
      }
    }

    default boolean isRegXxOrYy() {
      return isRegXx() || isRegYy();
    }

    default boolean isZeropage() {
      return this instanceof ZeroPage;
    }

    default boolean isRegAa() {
      return this instanceof Register register && Reg.AA.equals(register.reg());
    }

    default boolean isRegYy() {
      return this instanceof Register register && Reg.YY.equals(register.reg());
    }

    default boolean isMainMem() {
      return this instanceof MainMemory;
    }

    default boolean isZpOrMainMem() {
      return this.isZeropage() || this.isMainMem();
    }

    default boolean isAnyMem() {
      return this instanceof MemPlace;
    }

    default boolean isAny() {
      return this instanceof Any;
    }

    default boolean isU8() {
      return type() instanceof Type.Basic basic && basic.equals(Type.Basic.U8);
    }

    default boolean isU16() {
      return type() instanceof Type.Basic basic && basic.equals(Type.Basic.U16);
    }

    default boolean isDerefIdx() {
      return this instanceof DerefIdx;
    }

    default boolean isIndirectZpY() {
      return this instanceof IndirectZpY;
    }

    default boolean isAbsXy() {
      return this instanceof AbsXy;
    }

    default boolean isAbsXx() {
      return this instanceof AbsXx;
    }

    default boolean isAbsYy() {
      return this instanceof AbsYy;
    }

    default boolean isConst() {
      return this instanceof Constant;
    }

    default boolean isRegXx() {
      return this instanceof Register register && Reg.XX.equals(register.reg());
    }

    default boolean isReg() {
      return isRegAa() || isRegXx() || isRegYy();
    }

    default Reg reg() {
      return this instanceof Register register ? register.reg() : null;
    }

    static List<Place> listOf(Place input, List<Place> extraInputs) {
      final ArrayList<Place> inputs = new ArrayList<>();
      inputs.add(input);
      if (extraInputs != null) inputs.addAll(extraInputs);
      return Collections.unmodifiableList(inputs);
    }

    static List<Place> listOf(List<Place> input1, List<Place> input2) {
      final ArrayList<Place> inputs = new ArrayList<>(input1);
      if (input2 != null) inputs.addAll(input2);
      return Collections.unmodifiableList(inputs);
    }

    enum Reg {
      AA("lda", "sta"),
      XX("ldx", "stx"),
      YY("ldy", "sty"),
      ZZ("ldz", "stz"),
      ;

      final String load;
      final String store;

      Reg(String load, String store) {
        this.load = load;
        this.store = store;
      }

      public String loadMnemonic() {
        return load;
      }

      public String storeMnemonic() {
        return store;
      }
    }

    record Register(PlaceId id, Type type, Reg reg) implements Place {
      public String toString() {
        return ModelPretty.pretty(this);
      }
    }

    record Constant(PlaceId id, Type type, PlaceExpr valueExpr) implements Place {
      public String toString() {
        return ModelPretty.pretty(this);
      }
    }

    record DerefIdx(PlaceId id, Type type) implements Place {
      public String toString() {
        return ModelPretty.pretty(this);
      }
    }

    record Deref(PlaceId id, Type type) implements Place {
      public String toString() {
        return ModelPretty.pretty(this);
      }
    }

    sealed interface MemPlace extends Place
        permits Place.MainMemory, Place.ZeroPage, Place.IndirectZpY, Place.AbsXy {
      PlaceExpr addrExpr();
    }

    record MainMemory(PlaceId id, Type type, PlaceExpr addrExpr) implements MemPlace {
      public String toString() {
        return ModelPretty.pretty(this);
      }
    }

    record ZeroPage(PlaceId id, Type type, PlaceExpr addrExpr) implements MemPlace {
      public String toString() {
        return ModelPretty.pretty(this);
      }
    }

    record IndirectZpY(PlaceId id, Type type, PlaceExpr addrExpr, PlaceId yyPlaceId)
        implements MemPlace {
      public String toString() {
        return ModelPretty.pretty(this);
      }
    }

    sealed interface AbsXy extends MemPlace permits AbsXx, AbsYy {
      PlaceId idxPlaceId();
    }

    record AbsXx(PlaceId id, Type type, PlaceExpr addrExpr, PlaceId idxPlaceId) implements AbsXy {
      public String toString() {
        return ModelPretty.pretty(this);
      }
    }

    record AbsYy(PlaceId id, Type type, PlaceExpr addrExpr, PlaceId idxPlaceId) implements AbsXy {
      public String toString() {
        return ModelPretty.pretty(this);
      }
    }

    record Any(PlaceId id, Type type) implements Place {}
  }

  /**
   * Uniquely identifies an operation within a {@link Model}.
   *
   * @param id The integer ID
   */
  record OperationId(int id) {}

  /**
   * An operation that modifies data in output places based on data from input places. Examples of
   * operations are MOV which move data from one place to another and ADC which adds data from three
   * inputs (a, b, carry) to produce two outputs (a+b, carry).
   *
   * <p>The goal of rewriting is to find the optimal model where all operations can be converted
   * directly to legal ASM instructions and linearized in a way where they do not clobber each
   * other's places.
   *
   * <p>Low-level operations are directly convertible to an ASM instruction or close to being
   * convertible to an ASM instruction. They put many restrictions on which places they work with.
   * For instance ADC only works with the A-register and the C-flag.
   *
   * <p>High-level operations are not directly convertible to ASM instructions and requires
   * rewriting. They are much more flexible in which places they work with. For instance ADD can
   * work with all input places that have the same type.
   *
   * @param id Uniquely identifies the operation within the model.
   * @param op The semantics of the operation.
   * @param outputs The output places that holds the results of performing the operation.
   * @param inputs The input places that holds the inputs for performing the operation.
   * @param asm If the operation is convertible to ASM this holds the ASM instruction.
   */
  public record Operation(
      OperationId id, Op op, List<Place> outputs, List<Place> inputs, Fragment.Instruction asm) {

    public Operation {
      Objects.requireNonNull(id);
      Objects.requireNonNull(op);
      Objects.requireNonNull(inputs);
      Objects.requireNonNull(outputs);
    }

    public Operation(OperationId id, Op op, Place output, Place input) {
      this(id, op, List.of(output), List.of(input), null);
    }

    public Operation(OperationId id, Op op, Place output, Place inputLeft, Place inputRight) {
      this(id, op, List.of(output), List.of(inputLeft, inputRight), null);
    }

    /**
     * Is the operation directly convertible to ASM.
     *
     * @return true if the operation can be directly converted to ASM.
     */
    public boolean isAsm() {
      return asm != null;
    }

    public boolean hasOutput(Place place) {
      return outputs().contains(place);
    }

    public Place output() {
      if (outputs().size() != 1) throw new RuntimeException("Operation must have 1 output");
      return outputs().get(0);
    }

    public Place input() {
      if (inputs().size() != 1) throw new RuntimeException("Operation must have 1 input");
      return inputs().get(0);
    }

    public Place inputLeft() {
      if (inputs().size() != 2) throw new RuntimeException("Operation must have 2 inputs");
      return inputs().get(0);
    }

    public Place inputRight() {
      if (inputs().size() != 2) throw new RuntimeException("Operation must have 2 inputs");
      return inputs().get(1);
    }

    /**
     * Get both inputs and outputs.
     *
     * @return Inputs and outputs
     */
    private List<Place> getPlaces() {
      List<Place> places = new ArrayList<>();
      places.addAll(inputs());
      places.addAll(outputs());
      return places;
    }

    /**
     * Get all places referenced by the model. This is all places referenced by abs_xy/ind_zpy
     * places. It does not include the inputs/output places.
     *
     * @return All places referenced by this operation.
     */
    public List<PlaceId> getReferencedPlaces() {
      // Also add all referenced places
      return new ArrayList<>(
          getPlaces().stream().map(Place::getReferenced).flatMap(Collection::stream).toList());
    }

    /**
     * Get all places used by the operation. This includes all inputs, outputs and places referenced
     * by abs_xy/ind_zpy places..
     *
     * @return All places used by this operation.
     */
    public List<PlaceId> getAllPlaces() {
      List<PlaceId> allPlaces = new ArrayList<>();
      // Find all inputs and outputs
      allPlaces.addAll(getPlaces().stream().map(Place::id).toList());
      // Also add all referenced places
      allPlaces.addAll(
          getPlaces().stream().map(Place::getReferenced).flatMap(Collection::stream).toList());
      return allPlaces;
    }

    public boolean isMov() {
      return Op.MOV.equals(this.op()) && this.inputs.size() == 1;
    }

    public boolean isBinSimple() {
      return Op.BINARY_SIMPLE_MNEMONICS.containsKey(this.op());
    }

    public boolean isLoad() {
      return isMov()
          && input().isU8()
          && (input().isAnyMem() || input().isConst())
          && output().isReg();
    }

    public boolean isStore() {
      return isMov() && output().isU8() && output().isAnyMem() && input().isReg();
    }

    public boolean isDeref() {
      return Op.DEREF.equals(this.op());
    }

    public boolean isDerefIdx() {
      return Op.DEREF_IDX.equals(this.op());
    }

    public boolean isInc() {
      return Op.INC.equals(this.op());
    }

    public boolean isDec() {
      return Op.DEC.equals(this.op());
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o instanceof Operation that) {
        return id.equals(that.id());
      } else {
        return false;
      }
    }

    @Override
    public int hashCode() {
      return Objects.hash(id);
    }

    @Override
    public String toString() {
      return ModelPretty.pretty(this);
    }
  }

  /** Operation semantics. */
  public enum Op {
    /** Move a value. */
    MOV,
    /** ptr . */
    DEREF,
    /** ptr[idx] . */
    DEREF_IDX,
    /** bitwise AND. */
    BIN_AND,
    /** bitwise OR. */
    BIN_OR,
    /** bitwise XOR. */
    BIN_XOR,
    /** increase. */
    INC,
    /** decrease. */
    DEC,
    ;
    /** Maps simple binary operations to the ASM mnemonic. */
    public static final Map<Op, String> BINARY_SIMPLE_MNEMONICS =
        Map.of(
            BIN_AND, "and",
            BIN_OR, "ora",
            BIN_XOR, "eor");
  }

  // To consider:
  // What is the right granularity:
  // - Lda(in, aa) - input can be constant, memory or maybe even indexed memory. Output is the
  // A-register.
  // - LdaImm(const,aa) - input is a constant U8/I8. Output is the A-register.
  // - LdaAbsX(mem, xx, aa) - inputs are memory and the X-register
  // Are operations fully typed or untyped?
  // - Mov(in,out) untyped requires in and out to be same type
  // - MovU8(in, out) typed requires in and out to be U8
  // Are operations signed/unsigned - or not caring about signs?
  // - (LdaU/LdaI vs Lda)
  // Is type casting an explicit operation or implicit?
  // - For instance when handling pointers versus U16.
  // How are flags like N, Z modelled for instructions such as LDA ?
  // How is pointer addition and deref rewritten to indexed addressing ?
  // - high level is "v1=*(v2+idx)" or "v1=v2[idx]" while low level is "LDA mem,x"

}
