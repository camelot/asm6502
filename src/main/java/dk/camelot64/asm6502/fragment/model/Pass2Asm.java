package dk.camelot64.asm6502.fragment.model;

import dk.camelot64.asm6502.fragment.Forger;
import dk.camelot64.asm6502.fragment.model.pass2.*;
import java.util.List;

/** Rewrites a lowered model to ASM if possible. */
public class Pass2Asm {

  /**
   * Rewrite all operations of a model to ASM operations, if possible.
   *
   * @param model The model to convert
   * @return The model, where all operations are converted to ASM (if possible).
   */
  public static Model rewriteToAsm(Model model, Forger.IdAllocator modelIdAllocator) {
    SpecificRewrite asmRewrite = getNextAsmRewrite(model);
    while (asmRewrite != null) {
      model = asmRewrite.rewrite(model, modelIdAllocator.nextId());
      ModelInvariants.assertModel(model, asmRewrite);
      asmRewrite = getNextAsmRewrite(model);
    }
    return model;
  }

  /**
   * Get the next ASM rewrite for a model.
   *
   * @param model The model to consider.
   * @return The next ASM rewrite that can be performed on the model.
   */
  private static SpecificRewrite getNextAsmRewrite(Model model) {
    for (Model.Operation operation : model.operations()) {
      for (Rewrite.OperationRule rule : REWRITES) {
        if (rule.match(operation, model))
          return new SpecificRewrite.RewriteOperation(operation, rule);
      }
    }
    return null;
  }

  /** The rewrites used for rewriting the model operations to ASM. */
  static final List<Rewrite.OperationRule> REWRITES =
      List.of(
          new TransferToAsm(),
          new IncDecToAsm(),
          new LoadToAsm(),
          new StoreToAsm(),
          new BinSimpleToAsm());
}
