package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Rewrite MOV(ABS_XY, U8) to use intermediate MOV(AA, U8), MOV(ABS_XY, AA). */
public class MovToAbsXyThroughAa implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isMov()
        && operation.input().isU8()
        && operation.output().isU8()
        && !operation.input().isRegAa()
        && (operation.output().isAbsXx() || operation.output().isAbsYy());
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    final Model.Place.Register aaPlace = builder.addPlaceAa(operation.input().type());
    builder.addOperation(Model.Op.MOV, aaPlace, operation.input());
    builder.addOperation(Model.Op.MOV, operation.output(), aaPlace);
    builder.remove(operation);
    return builder.model();
  }
}
