package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Rewrite BIN(U8, U8, U8) to use intermediate BIN(AA, U8, U8), MOV(U8, AA). */
public class BinOutputThroughAa implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isBinSimple()
        && operation.inputLeft().isU8()
        && operation.inputRight().isU8()
        && operation.output().isU8()
        && !operation.output().isRegAa();
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    final Model.Place.Register aaPlace = builder.addPlaceAa(operation.output().type());
    builder.addOperation(operation.op(), aaPlace, operation.inputLeft(), operation.inputRight());
    builder.addOperation(Model.Op.MOV, operation.output(), aaPlace);
    builder.remove(operation);
    return builder.model();
  }
}
