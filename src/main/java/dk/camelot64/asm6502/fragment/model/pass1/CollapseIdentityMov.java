package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Collapse mov that moves between equivalent places. */
public class CollapseIdentityMov implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isMov() && isIdentity(operation.input(), operation.output());
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    ModelBuilder builder = new ModelBuilder(newModelId, model);
    builder.replace(operation.input(), operation.output());
    builder.remove(operation);
    builder.remove(operation.input());
    return builder.model();
  }

  private boolean isIdentity(Model.Place o1, Model.Place o2) {
    if (!o1.type().equals(o2.type())) return false;
    if (!o1.getClass().equals(o2.getClass())) return false;
    // Notice that places can be equivalent even if they differ on isLValue()
    if (o1 instanceof Model.Place.Register reg1) {
      Model.Place.Register reg2 = (Model.Place.Register) o2;
      return reg1.reg().equals(reg2.reg());
    } else {
      return false;
    }
  }
}
