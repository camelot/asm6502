package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;
import java.util.List;

/** Collapse sibling operations that calculate the same R-values. */
public class CollapseEquivalentSiblings implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return (operation.isMov() || operation.isDerefIdx() || operation.isInc() || operation.isDec())
        && findEquivalentSibling(operation, model) != null;
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final Model.Operation sibling = findEquivalentSibling(operation, model);
    ModelBuilder builder = new ModelBuilder(newModelId, model);
    builder.remove(operation);
    final List<Model.Place> out1 = operation.outputs();
    final List<Model.Place> out2 = sibling.outputs();
    for (int i = 0; i < out1.size(); i++) {
      builder.replace(out1.get(i), out2.get(i));
      builder.remove(out1.get(i));
    }
    return builder.model();
  }

  Model.Operation findEquivalentSibling(Model.Operation operation, Model model) {
    final List<Model.Operation> siblings =
        model.find(
            op ->
                operation != op
                    && op.getClass().equals(operation.getClass())
                    && op.inputs().equals(operation.inputs())
                    && isEquivalent(operation.outputs(), op.outputs()));
    if (siblings.size() == 0) return null;
    return siblings.get(0);
  }

  private boolean isEquivalent(List<Model.Place> out1, List<Model.Place> out2) {
    if (out1.size() != out2.size()) return false;
    for (int i = 0; i < out1.size(); i++) {
      final Model.Place o1 = out1.get(i);
      final Model.Place o2 = out2.get(i);
      if (!isEquivalent(o1, o2)) return false;
    }
    return true;
  }

  private boolean isEquivalent(Model.Place o1, Model.Place o2) {
    if (!o1.type().equals(o2.type())) return false;
    if (!o1.getClass().equals(o2.getClass())) return false;
    if (o1.id().lValue() != o2.id().lValue()) return false;
    if (o1 instanceof Model.Place.Register reg1) {
      Model.Place.Register reg2 = (Model.Place.Register) o2;
      return reg1.reg().equals(reg2.reg());
    } else {
      return true;
    }
  }
}
