package dk.camelot64.asm6502.fragment.model;

import dk.camelot64.asm6502.fragment.Fragment;
import dk.camelot64.asm6502.fragment.model.Model.Operation;
import dk.camelot64.asm6502.fragment.model.Model.Place;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/** Used to build a fragment {@link Model}. */
public final class ModelBuilder {

  private final int newModelId;

  private List<Place> places;
  private List<Operation> operations;
  private final IdAllocator idAllocator;

  private Model.Operation addAsmOperation(
      Fragment.Instruction asm, Model.Op op, List<Place> outputs, List<Place> inputs) {
    final Model.Operation asmOperation =
        new Model.Operation(idAllocator.nextOperationId(), op, outputs, inputs, asm);
    add(asmOperation);
    return asmOperation;
  }

  public void addAsmOperation(Fragment.Instruction asm, Operation operation) {
    addAsmOperation(asm, operation.op(), operation.outputs(), operation.inputs());
  }

  public void addAsmOperation(
      Fragment.Instruction asm, Operation operation, List<Place> newInputs) {
    addAsmOperation(asm, operation.op(), operation.outputs(), newInputs);
  }

  public Model.Operation addOperation(Model.Op op, List<Place> outputs, List<Place> inputs) {
    final Model.Operation highOperation =
        new Model.Operation(idAllocator.nextOperationId(), op, outputs, inputs, null);
    add(highOperation);
    return highOperation;
  }

  public Model.Operation addOperation(Model.Op op, Place output, Place input) {
    return addOperation(op, List.of(output), List.of(input));
  }

  public Model.Operation addOperation(
      Model.Op op, Place output, Place inputLeft, Place inputRight) {
    return addOperation(op, List.of(output), List.of(inputLeft, inputRight));
  }

  public List<Operation> operations() {
    return operations;
  }

  public List<Operation> find(Predicate<Operation> predicate) {
    return operations.stream().filter(predicate).toList();
  }

  /** Remove any places that are no longer used by any operation. */
  public void removeUnusedPlaces() {
    final Set<Model.PlaceId> allReferencedPlaces =
        this.operations.stream()
            .map(Operation::getAllPlaces)
            .flatMap(List::stream)
            .collect(Collectors.toSet());
    places.removeIf(place -> !allReferencedPlaces.contains(place.id()));
  }

  /** Allocator for place ID's. */
  public static class IdAllocator {
    int nextMemId;
    int nextConstId;
    int nextRegId;
    int nextOtherId;
    int nextOperationId;

    public IdAllocator(
        int nextMemId, int nextConstId, int nextRegId, int nextOtherId, int nextOperationId) {
      this.nextMemId = nextMemId;
      this.nextConstId = nextConstId;
      this.nextRegId = nextRegId;
      this.nextOtherId = nextOtherId;
      this.nextOperationId = nextOperationId;
    }

    public static IdAllocator from(Model model) {
      IdAllocator allocator = new IdAllocator(1, 1, 1, 1, 1);
      for (Place place : model.places()) {
        final Model.PlaceKind placeKind = place.id().kind();
        switch (placeKind) {
          case ZEROPAGE:
          case MAINMEMORY:
            allocator = max(allocator, new IdAllocator(place.id().idx() + 1, 1, 1, 1, 1));
            break;
          case CONSTANT:
            allocator = max(allocator, new IdAllocator(1, place.id().idx() + 1, 1, 1, 1));
          case REGISTER:
            allocator = max(allocator, new IdAllocator(1, 1, place.id().idx() + 1, 1, 1));
            break;
          case OTHER:
            allocator = max(allocator, new IdAllocator(1, 1, 1, place.id().idx() + 1, 1));
            break;
          default:
            throw new RuntimeException("Unknown place kind " + placeKind);
        }
      }
      for (Operation operation : model.operations()) {
        allocator = max(allocator, new IdAllocator(1, 1, 1, 1, operation.id().id() + 1));
      }
      return allocator;
    }

    public static IdAllocator max(IdAllocator alloc1, IdAllocator alloc2) {
      return new IdAllocator(
          Math.max(alloc1.nextMemId, alloc2.nextMemId),
          Math.max(alloc1.nextConstId, alloc2.nextConstId),
          Math.max(alloc1.nextRegId, alloc2.nextRegId),
          Math.max(alloc1.nextOtherId, alloc2.nextOtherId),
          Math.max(alloc1.nextOperationId, alloc2.nextOperationId));
    }

    public Model.PlaceId nextId(Model.PlaceKind kind, boolean lValue) {
      return switch (kind) {
        case ZEROPAGE, MAINMEMORY -> Model.PlaceId.of(kind, nextMemId++, lValue);
        case CONSTANT -> Model.PlaceId.of(kind, nextConstId++, lValue);
        case REGISTER -> Model.PlaceId.of(kind, nextRegId++, lValue);
        case OTHER -> Model.PlaceId.of(kind, nextOtherId++, lValue);
      };
    }

    public Model.OperationId nextOperationId() {
      return new Model.OperationId(nextOperationId++);
    }
  }

  /**
   * Create an empty model builder for creating a model from scratch.
   *
   * @param newModelId The ID of the new model being built
   * @param idAllocator The allocator used for IDs for new places
   */
  public ModelBuilder(int newModelId, IdAllocator idAllocator) {
    this.newModelId = newModelId;
    this.places = new ArrayList<>();
    this.operations = new ArrayList<>();
    this.idAllocator = idAllocator;
  }

  /**
   * Create a model builder for building a new model from an existing model.
   *
   * @param newModelId The ID of the new model being built.
   * @param model The existing model.
   */
  public ModelBuilder(int newModelId, Model model) {
    this.newModelId = newModelId;
    this.places = new ArrayList<>(model.places());
    this.operations = new ArrayList<>(model.operations());
    this.idAllocator = IdAllocator.from(model);
  }

  public Model model() {
    return new Model(
        newModelId, Collections.unmodifiableList(places), Collections.unmodifiableList(operations));
  }

  public Model.PlaceId newId(Model.PlaceKind kind, boolean lValue) {
    return idAllocator.nextId(kind, lValue);
  }

  /**
   * Finds a register RValue. Used to ensure that all RValue references to the same register in a
   * signature get the same ID.
   *
   * @param reg The register
   * @return The RValue place containing the register. Null if not present.
   */
  public Model.Place findRegisterRVal(Model.Place.Reg reg) {
    for (Place place : places) {
      if (place instanceof Place.Register register
          && register.reg().equals(reg)
          && !register.id().lValue()) return place;
    }
    return null;
  }

  public void add(Operation operation) {
    this.operations.add(operation);
  }

  public Place.Register addPlaceAa(final Model.Type type) {
    final Place.Register place =
        new Place.Register(newId(Model.PlaceKind.REGISTER, false), type, Place.Reg.AA);
    add(place);
    return place;
  }

  public Place.Register addPlaceXx(final Model.Type type) {
    final Place.Register place =
        new Place.Register(newId(Model.PlaceKind.REGISTER, false), type, Place.Reg.XX);
    add(place);
    return place;
  }

  public Place.Register addPlaceYy(final Model.Type type) {
    final Place.Register place =
        new Place.Register(newId(Model.PlaceKind.REGISTER, false), type, Place.Reg.YY);
    add(place);
    return place;
  }

  public Place addPlaceAny(Model.Type type) {
    return add(new Place.Any(newId(Model.PlaceKind.OTHER, false), type));
  }

  public Place addPlaceZp(final Model.Type type) {
    final Model.PlaceId zpId = this.newId(Model.PlaceKind.ZEROPAGE, false);
    final Place.ZeroPage zpPlace = new Place.ZeroPage(zpId, type, new Model.PlaceExpr.Id(zpId));
    return this.add(zpPlace);
  }

  /**
   * Specifies properties for a byte place. Used for finding existing byte places when possible.
   *
   * @param clazz The {@link Place} class.
   * @param type The place type (mostly U8)
   * @param addrExpr The address-expression (may be null).
   * @param idxExpr The index-expression (may be null).
   * @param byteOffset The byte offset.
   */
  record PlaceSpec(
      Class<? extends Place> clazz,
      boolean lValue,
      Model.Type type,
      Model.PlaceExpr addrExpr,
      Model.PlaceId idxId) {

    public static PlaceSpec from(Place place) {
      if (place instanceof Place.Constant constantPlace) {
        return new PlaceSpec(
            Place.Constant.class,
            constantPlace.id().lValue(),
            constantPlace.type(),
            constantPlace.valueExpr(),
            null);
      } else if (place instanceof Place.ZeroPage zpPlace) {
        return new PlaceSpec(
            Place.ZeroPage.class, zpPlace.id().lValue(), zpPlace.type(), zpPlace.addrExpr(), null);
      } else if (place instanceof Place.MainMemory memPlace) {
        return new PlaceSpec(
            Place.MainMemory.class,
            memPlace.id().lValue(),
            memPlace.type(),
            memPlace.addrExpr(),
            null);
      } else if (place instanceof Place.AbsXx absXxPlace) {
        return new PlaceSpec(
            Place.AbsXx.class,
            absXxPlace.id().lValue(),
            absXxPlace.type(),
            absXxPlace.addrExpr(),
            absXxPlace.idxPlaceId());
      } else if (place instanceof Place.AbsYy absYyPlace) {
        return new PlaceSpec(
            Place.AbsYy.class,
            absYyPlace.id().lValue(),
            absYyPlace.type(),
            absYyPlace.addrExpr(),
            absYyPlace.idxPlaceId());
      } else if (place instanceof Place.IndirectZpY indZpyPlace) {
        return new PlaceSpec(
            Place.IndirectZpY.class,
            indZpyPlace.id().lValue(),
            indZpyPlace.type(),
            indZpyPlace.addrExpr(),
            indZpyPlace.yyPlaceId());
      } else if (place instanceof Place.Register regPlace) {
        return new PlaceSpec(
            Place.Register.class, regPlace.id().lValue(), regPlace.type(), null, null);
      } else if (place instanceof Place.Any anyPlace) {
        return new PlaceSpec(Place.Any.class, anyPlace.id().lValue(), anyPlace.type(), null, null);
      } else {
        throw new RuntimeException("Place not supported " + place);
      }
    }

    public static PlaceSpec fromByteOffset(
        Place placeU16, int byteOffset, boolean lValue, ModelBuilder builder) {
      if (placeU16 instanceof Place.ZeroPage) {
        return new PlaceSpec(
            Place.ZeroPage.class,
            false,
            Model.Type.Basic.U8,
            Model.PlaceExpr.byteOf(placeU16.id(), byteOffset),
            null);
      } else if (placeU16 instanceof Place.MainMemory) {
        return new PlaceSpec(
            Place.MainMemory.class,
            lValue,
            Model.Type.Basic.U8,
            Model.PlaceExpr.byteOf(placeU16.id(), byteOffset),
            null);
      } else if (placeU16 instanceof Place.AbsXx absXx) {
        final Model.PlaceExpr.Id addrExpr = (Model.PlaceExpr.Id) absXx.addrExpr();
        return new PlaceSpec(
            Place.AbsXx.class,
            lValue,
            Model.Type.Basic.U8,
            Model.PlaceExpr.byteOf(addrExpr.placeId(), byteOffset),
            absXx.idxPlaceId());
      } else if (placeU16 instanceof Place.AbsYy absYy) {
        final Model.PlaceExpr.Id addrExpr = (Model.PlaceExpr.Id) absYy.addrExpr();
        return new PlaceSpec(
            Place.AbsYy.class,
            lValue,
            Model.Type.Basic.U8,
            Model.PlaceExpr.byteOf(addrExpr.placeId(), byteOffset),
            absYy.idxPlaceId());
      } else if (placeU16 instanceof Place.IndirectZpY indZpY) {
        final Model.PlaceExpr.Id addrExpr = (Model.PlaceExpr.Id) indZpY.addrExpr();
        Place.Register placeYy = (Place.Register) builder.get(indZpY.yyPlaceId());
        if (byteOffset != 0) {
          // Add an iny and a new YY-place
          // TODO: Handle U32 with multiple INY's
          // TODO: Handle multiple usages of the same U16 without creating multiple INYs
          final Place.Register placeYyInc = builder.addPlaceYy(Model.Type.Basic.U8);
          builder.addOperation(Model.Op.INC, placeYyInc, placeYy);
          placeYy = placeYyInc;
        }
        return new PlaceSpec(
            Place.IndirectZpY.class,
            lValue,
            Model.Type.Basic.U8,
            Model.PlaceExpr.byteOf(addrExpr.placeId(), byteOffset),
            placeYy.id());
      } else if (placeU16 instanceof Place.Constant) {
        return new PlaceSpec(
            Place.Constant.class,
            lValue,
            Model.Type.Basic.U8,
            Model.PlaceExpr.byteOf(placeU16.id(), byteOffset),
            null);
      } else if (placeU16 instanceof Place.Any) {
        return new PlaceSpec(Place.Any.class, lValue, Model.Type.Basic.U8, null, null);
      } else {
        throw new RuntimeException("Place not supported " + placeU16);
      }
    }

    public Place add(ModelBuilder builder) {
      if (clazz.isAssignableFrom(Place.ZeroPage.class)) {
        return builder.add(
            new Place.ZeroPage(builder.newId(Model.PlaceKind.ZEROPAGE, lValue), type, addrExpr));
      } else if (clazz.isAssignableFrom(Place.MainMemory.class)) {
        return builder.add(
            new Place.MainMemory(
                builder.newId(Model.PlaceKind.MAINMEMORY, lValue), type, addrExpr));
      } else if (clazz.isAssignableFrom(Place.AbsXx.class)) {
        return builder.add(
            new Place.AbsXx(builder.newId(Model.PlaceKind.OTHER, lValue), type, addrExpr, idxId));
      } else if (clazz.isAssignableFrom(Place.AbsYy.class)) {
        return builder.add(
            new Place.AbsYy(builder.newId(Model.PlaceKind.OTHER, lValue), type, addrExpr, idxId));
      } else if (clazz.isAssignableFrom(Place.IndirectZpY.class)) {
        return builder.add(
            new Place.IndirectZpY(
                builder.newId(Model.PlaceKind.OTHER, lValue), type, addrExpr, idxId));
      } else if (clazz.isAssignableFrom(Place.Constant.class)) {
        return builder.add(
            new Place.Constant(builder.newId(Model.PlaceKind.CONSTANT, lValue), type, addrExpr));
      } else if (clazz.isAssignableFrom(Place.Any.class)) {
        return builder.add(new Place.Any(builder.newId(Model.PlaceKind.OTHER, lValue), type));
      } else {
        throw new RuntimeException("Unknown place class " + this.toString());
      }
    }

    public Place find(ModelBuilder builder) {
      return builder.places.stream()
          .filter(place -> PlaceSpec.from(place).equals(this))
          .findFirst()
          .orElse(null);
    }
  }

  public Place addBytePlace(Place placeU16, boolean lValue, int byteOffset) {
    PlaceSpec placeSpec = PlaceSpec.fromByteOffset(placeU16, byteOffset, lValue, this);
    return placeSpec.add(this);
  }

  public Place get(Model.PlaceId placeId) {
    return places.stream().filter(place -> placeId.equals(place.id())).findFirst().orElse(null);
  }

  public Place add(Place place) {
    this.places.add(place);
    return place;
  }

  public void remove(Operation operation) {
    this.operations.remove(operation);
  }

  public void remove(Place place) {
    this.places.remove(place);
  }

  /**
   * Replace one place with another place.
   *
   * @param oldPlace The place to remove
   * @param newPlace The place to add instead. (Place ID must be the same)
   */
  public void replace(Place oldPlace, Place newPlace) {
    this.places =
        new ArrayList<>(
            this.places.stream().map(place -> replace(place, oldPlace, newPlace)).toList());
    this.operations =
        new ArrayList<>(
            this.operations.stream()
                .map(operation -> replace(operation, oldPlace, newPlace))
                .toList());
  }

  private Operation replace(Operation op, Place oldPlace, Place newPlace) {
    if (!op.getAllPlaces().contains(oldPlace.id())) {
      return op;
    }
    return new Model.Operation(
        op.id(),
        op.op(),
        op.outputs().stream().map(place -> replace(place, oldPlace, newPlace)).toList(),
        op.inputs().stream().map(place -> replace(place, oldPlace, newPlace)).toList(),
        op.asm());
  }

  private static Place replace(Place place, Place oldPlace, Place newPlace) {
    if (place.id().equals(oldPlace.id())) return newPlace;
    if (place instanceof Place.AbsXx absXx) {
      if (absXx.idxPlaceId().equals(oldPlace.id())) {
        return new Place.AbsXx(absXx.id(), absXx.type(), absXx.addrExpr(), newPlace.id());
      } else if (absXx.addrExpr().placeId().equals(oldPlace.id())) {
        final Model.PlaceExpr replacedExpr = replace(absXx.addrExpr(), newPlace);
        return new Place.AbsXx(absXx.id(), absXx.type(), replacedExpr, absXx.idxPlaceId());
      }
    } else if (place instanceof Place.AbsYy absYy) {
      if (absYy.idxPlaceId().equals(oldPlace.id())) {
        return new Place.AbsYy(absYy.id(), absYy.type(), absYy.addrExpr(), newPlace.id());
      } else if (absYy.addrExpr().placeId().equals(oldPlace.id())) {
        final Model.PlaceExpr replacedExpr = replace(absYy.addrExpr(), newPlace);
        return new Place.AbsYy(absYy.id(), absYy.type(), replacedExpr, absYy.idxPlaceId());
      }
    } else if (place instanceof Place.IndirectZpY indZpY) {
      if (indZpY.yyPlaceId().equals(oldPlace.id())) {
        return new Place.IndirectZpY(indZpY.id(), indZpY.type(), indZpY.addrExpr(), newPlace.id());
      } else if (indZpY.addrExpr().placeId().equals(oldPlace.id())) {
        final Model.PlaceExpr replacedExpr = replace(indZpY.addrExpr(), newPlace);
        return new Place.IndirectZpY(indZpY.id(), indZpY.type(), replacedExpr, indZpY.yyPlaceId());
      }
    }

    return place;
  }

  private static Model.PlaceExpr replace(Model.PlaceExpr addrExpr, Place newPlace) {
    if (addrExpr instanceof Model.PlaceExpr.Id) {
      return new Model.PlaceExpr.Id(newPlace.id());
    } else if (addrExpr instanceof Model.PlaceExpr.ByteOf exprByte) {
      return new Model.PlaceExpr.ByteOf(newPlace.id(), exprByte.offset());
    } else {
      throw new RuntimeException("Unknown place expression " + addrExpr);
    }
  }
}
