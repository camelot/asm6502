package dk.camelot64.asm6502.fragment.model.pass2;

import dk.camelot64.asm6502.fragment.*;
import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;
import java.util.Map;

/** Rewrite MOV(Reg, Reg) to TAX/TXA/TAY/TYA. */
public class TransferToAsm implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return !operation.isAsm()
        && operation.isMov()
        && operation.output().isReg()
        && operation.input().isReg()
        && !operation.input().reg().equals(operation.output().reg());
  }

  record RegPair(Model.Place.Reg regIn, Model.Place.Reg regOut) {
    public RegPair(Model.Operation operation) {
      this(operation.input().reg(), operation.output().reg());
    }
  }

  static Map<RegPair, String> TRANSFER_MNEMONIC =
      Map.of(
          new RegPair(Model.Place.Reg.AA, Model.Place.Reg.XX), "tax",
          new RegPair(Model.Place.Reg.AA, Model.Place.Reg.YY), "tay",
          new RegPair(Model.Place.Reg.XX, Model.Place.Reg.AA), "txa",
          new RegPair(Model.Place.Reg.YY, Model.Place.Reg.AA), "tya");

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final String mnemonic = TRANSFER_MNEMONIC.get(new RegPair(operation));
    final Fragment.Instruction asm = new Fragment.Instruction(mnemonic);
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    builder.addAsmOperation(asm, operation);
    builder.remove(operation);
    return builder.model();
  }
}
