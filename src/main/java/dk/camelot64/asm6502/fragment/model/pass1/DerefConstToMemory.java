package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Rewrite DEREF(CONST) to MEM place. */
public class DerefConstToMemory implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isDeref() && operation.input().isConst();
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    ModelBuilder builder = new ModelBuilder(newModelId, model);
    final Model.Place.MainMemory memPlace =
        new Model.Place.MainMemory(
            operation.input().id(),
            operation.output().type(),
            Model.PlaceExpr.id(operation.input().id()));
    builder.replace(operation.output(), memPlace);
    builder.remove(operation);
    builder.remove(operation.input());
    return builder.model();
  }
}
