package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Rewrite MOV(ZPY, U8) to use intermediate MOV(AA, U8), MOV(ZPY, AA). */
public class MovToZpyThroughAa implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isMov()
        && operation.input().isU8()
        && operation.output().isU8()
        && !operation.input().isRegAa()
        && operation.output().isIndirectZpY();
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    final Model.Place.Register aaPlace = builder.addPlaceAa(operation.input().type());
    builder.addOperation(Model.Op.MOV, aaPlace, operation.input());
    builder.addOperation(Model.Op.MOV, operation.output(), aaPlace);
    builder.remove(operation);
    return builder.model();
  }
}
