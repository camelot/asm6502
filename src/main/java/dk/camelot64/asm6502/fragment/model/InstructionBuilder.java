package dk.camelot64.asm6502.fragment.model;

import dk.camelot64.asm6502.fragment.Fragment;

public class InstructionBuilder {

  /**
   * Create an ASM instruction with appropriate addressing mode and operands for a fragment.
   *
   * @param mnemonic The instruction mnemonic
   * @param place The input/output place to address with the instruction. Ths place type and data is
   *     used to determine the addressing mode and operand.
   * @return The new instruction.
   */
  public static Fragment.Instruction instruction(String mnemonic, Model.Place place) {
    return new Fragment.Instruction(mnemonic, addressingMode(place), fragmentExpr(place));
  }

  /**
   * Get the addressing mode needed to represent an abs,x/y instruction.
   *
   * @param place The abs_xy place
   * @return The appropriate addressing mode
   */
  public static Fragment.AddressingMode addressingMode(Model.Place place) {
    if (place instanceof Model.Place.AbsXx) {
      return Fragment.AddressingMode.ABS_X;
    } else if (place instanceof Model.Place.AbsYy) {
      return Fragment.AddressingMode.ABS_Y;
    } else if (place instanceof Model.Place.IndirectZpY) {
      return Fragment.AddressingMode.IND_Y;
    } else if (place instanceof Model.Place.ZeroPage) {
      return Fragment.AddressingMode.ABS;
    } else if (place instanceof Model.Place.MainMemory) {
      return Fragment.AddressingMode.ABS;
    } else if (place instanceof Model.Place.Constant) {
      return Fragment.AddressingMode.IMM;
    } else {
      throw new RuntimeException("Unsupported place type" + place);
    }
  }

  /**
   * Create the ASM fragment expression used to reference a place correctly. This includes handling
   * byte offsets used for performing 16-bit operations.
   *
   * @param place The memory place to create ASM for
   * @return The ASM fragment expression used to reference the place.
   */
  public static Fragment.Expr fragmentExpr(Model.Place place) {
    if (place instanceof Model.Place.IndirectZpY indZpy) {
      final Model.PlaceExpr addrExpr = indZpy.addrExpr();
      if (addrExpr instanceof Model.PlaceExpr.Id exprId) {
        return new Fragment.Expr.Replace(exprId.placeId().replaceName());
      } else if (addrExpr instanceof Model.PlaceExpr.ByteOf exprByte) {
        // Byte offsets are handled by adding LDY/INY directly in the code - so no handling here.
        return new Fragment.Expr.Replace(exprByte.placeId().replaceName());
      } else {
        throw new RuntimeException("Unknown place expression " + place);
      }
    } else if (place instanceof Model.Place.MemPlace memPlace) {
      // ZeroPage, MainMem or AbsXy place - handled by +byteOffset
      final Model.PlaceExpr addrExpr = memPlace.addrExpr();
      if (addrExpr instanceof Model.PlaceExpr.Id exprId) {
        return new Fragment.Expr.Replace(exprId.placeId().replaceName());
      } else if (addrExpr instanceof Model.PlaceExpr.ByteOf exprByte) {
        return new Fragment.Expr.Binary(
            Fragment.Expr.BinOp.PLUS,
            new Fragment.Expr.Replace(exprByte.placeId().replaceName()),
            new Fragment.Expr.Int(exprByte.offset()));
      } else {
        throw new RuntimeException("Unknown place expression " + place);
      }
    } else if (place instanceof Model.Place.Constant constPlace) {
      final Model.PlaceExpr valueExpr = constPlace.valueExpr();
      if (valueExpr instanceof Model.PlaceExpr.Id exprId) {
        return new Fragment.Expr.Replace(exprId.placeId().replaceName());
      } else if (valueExpr instanceof Model.PlaceExpr.ByteOf exprByte) {
        if (exprByte.offset() == 0) {
          return new Fragment.Expr.Unary(
              Fragment.Expr.UnOp.LOW, new Fragment.Expr.Replace(exprByte.placeId().replaceName()));
        } else if (exprByte.offset() == 1) {
          return new Fragment.Expr.Unary(
              Fragment.Expr.UnOp.HIGH, new Fragment.Expr.Replace(exprByte.placeId().replaceName()));
        } else {
          throw new RuntimeException(
              "Byte " + exprByte.offset() + " const expression not supported");
        }
      } else {
        throw new RuntimeException("Unknown place expression " + place);
      }
    } else {
      throw new RuntimeException("Unsupported place type" + place);
    }
  }
}
