package dk.camelot64.asm6502.fragment.model.pass2;

import dk.camelot64.asm6502.fragment.Fragment;
import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;
import java.util.Map;

/** Rewrite INC(Reg)/DEC(Reg) to INY/INX. */
public class IncDecToAsm implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return !operation.isAsm()
        && (operation.isInc() || operation.isDec())
        && operation.output().isReg()
        && operation.input().isReg()
        && operation.input().reg().equals(operation.output().reg());
  }

  static Map<Model.Place.Reg, String> INC_MNEMONIC =
      Map.of(
          Model.Place.Reg.XX, "inx",
          Model.Place.Reg.YY, "iny");

  static Map<Model.Place.Reg, String> DEC_MNEMONIC =
      Map.of(
          Model.Place.Reg.XX, "dex",
          Model.Place.Reg.YY, "dey");

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final Model.Place.Reg reg = operation.input().reg();
    final String mnemonic = operation.isInc() ? INC_MNEMONIC.get(reg) : DEC_MNEMONIC.get(reg);
    final Fragment.Instruction asm = new Fragment.Instruction(mnemonic);
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    builder.addAsmOperation(asm, operation);
    builder.remove(operation);
    return builder.model();
  }
}
