package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Rewrite DEREF_IDX(const, XX/YY) to ABS_XX(const, XX) or ABS_YY(const, YY). */
public class DerefIdxToAbsXy implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {

    return operation.isDerefIdx()
        && operation.inputLeft().isConst()
        && operation.inputRight().isRegXxOrYy()
        && operation.output().isDerefIdx();
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final Model.Place.Constant constVal = (Model.Place.Constant) operation.inputLeft();
    Model.Place.Register regXy = (Model.Place.Register) operation.inputRight();
    final Model.Place newOutput =
        Model.Place.Reg.XX.equals(regXy.reg())
            ? new Model.Place.AbsXx(
                operation.output().id(),
                operation.output().type(),
                Model.PlaceExpr.id(constVal.id()),
                operation.inputRight().id())
            : new Model.Place.AbsYy(
                operation.output().id(),
                operation.output().type(),
                Model.PlaceExpr.id(constVal.id()),
                operation.inputRight().id());

    ModelBuilder builder = new ModelBuilder(newModelId, model);
    builder.remove(operation);
    builder.replace(operation.output(), newOutput);
    return builder.model();
  }
}
