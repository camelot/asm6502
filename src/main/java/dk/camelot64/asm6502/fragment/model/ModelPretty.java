package dk.camelot64.asm6502.fragment.model;

import dk.camelot64.asm6502.fragment.FragmentToKickAss;
import java.util.stream.Collectors;

/** Holds methods for pretty-printing Models. */
public class ModelPretty {

  public static String pretty(Model model) {
    StringBuilder builder = new StringBuilder();

    for (Model.Place place : model.places()) {
      if (!(place instanceof Model.Place.AbsXx)
          && !(place instanceof Model.Place.AbsYy)
          && !(place instanceof Model.Place.IndirectZpY)) {
        if (model.operationsWithOutput(place).isEmpty()) {
          builder.append("input ");
          builder.append(pretty(place));
          builder.append("\n");
        }
      }
    }

    for (Model.Operation operation : model.operations()) {
      builder.append(pretty(operation.output()));
      builder.append(" = ");
      String prettyId = "[" + operation.id().id() + "] ";
      builder.append(prettyId);
      builder.append(operation.op().name().toLowerCase());
      builder.append("(");
      builder.append(
          operation.inputs().stream()
              .map(ModelPretty::prettyParam)
              .collect(Collectors.joining(", ")));
      builder.append(")");
      if (operation.isAsm()) {
        builder.append(" = asm(");
        builder.append(FragmentToKickAss.toAsm(operation.asm()));
        builder.append(")");
      }
      builder.append("\n");
    }
    return builder.toString();
  }

  private static String prettyParam(Model.Place place) {
    if (place instanceof Model.Place.AbsYy
        || place instanceof Model.Place.AbsXx
        || place instanceof Model.Place.IndirectZpY) {
      return pretty(place);
    } else {
      return "@" + place.id().name();
    }
  }

  public static String pretty(Model.Place place) {
    return "@" + place.id().name() + ": " + pretty(place.type()) + " " + kind(place);
  }

  static String pretty(Model.Operation operation) {
    String prettyId = "[" + operation.id().id() + "] ";
    String highOpName = operation.op().name().toLowerCase();
    if (operation.isAsm()) {
      return prettyId + highOpName + "\n" + FragmentToKickAss.toAsm(operation.asm());
    } else {
      return prettyId + highOpName;
    }
  }

  static String kind(Model.Place place) {
    if (place instanceof Model.Place.Register register) {
      return register.reg().name().toLowerCase();
    } else if (place instanceof Model.Place.Any any) {
      return "any";
    } else if (place instanceof Model.Place.Constant constant) {
      return "const" + expr(constant);
    } else if (place instanceof Model.Place.MainMemory) {
      return "mem";
    } else if (place instanceof Model.Place.ZeroPage zp) {
      return "zp" + expr(zp);
    } else if (place instanceof Model.Place.Deref) {
      return "deref";
    } else if (place instanceof Model.Place.DerefIdx) {
      return "deref_idx";
    } else if (place instanceof Model.Place.IndirectZpY indirectZpY) {
      return "@" + expr(indirectZpY) + "[yy@" + indirectZpY.yyPlaceId().name() + "]";
    } else if (place instanceof Model.Place.AbsXx absXx) {
      return "@" + expr(absXx) + "[xx@" + absXx.idxPlaceId().name() + "]";
    } else if (place instanceof Model.Place.AbsYy Yy) {
      return "@" + expr(Yy) + "[yy@" + Yy.idxPlaceId().name() + "]";
    } else {
      throw new RuntimeException("Unknown place kind " + place);
    }
  }

  private static String expr(Model.Place place) {
    final Model.PlaceExpr expr;
    if (place instanceof Model.Place.Constant constPlace) {
      expr = constPlace.valueExpr();
    } else if (place instanceof Model.Place.MemPlace memPlace) {
      expr = memPlace.addrExpr();
    } else {
      throw new RuntimeException("Unknown place " + place);
    }
    if (expr instanceof Model.PlaceExpr.Id exprId) {
      if (exprId.placeId().equals(place.id())) {
        return "";
      } else {
        return " " + exprId.placeId().replaceName();
      }
    } else if (expr instanceof Model.PlaceExpr.ByteOf exprByte) {
      return " byte" + exprByte.offset() + "(" + exprByte.placeId().replaceName() + ")";
    } else {
      throw new RuntimeException("Unknown place expression " + expr);
    }
  }

  static String pretty(Model.Type type) {
    if (type instanceof Model.Type.Basic basic) {
      return basic.name().toLowerCase();
    } else if (type instanceof Model.Type.Pointer pointer) {
      return "*" + pretty(pointer.toType());
    } else {
      throw new RuntimeException("Unknown type " + type);
    }
  }
}
