package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Rewrite BIN(U8, U8, U8) to use intermediate MOV(AA, U8), BIN(U8, U8, AA). */
public class BinInputRightThroughAa implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isBinSimple()
        && operation.inputLeft().isU8()
        && operation.inputRight().isU8()
        && operation.output().isU8()
        && !operation.inputLeft().isRegAa()
        && !operation.inputRight().isRegAa();
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    final Model.Place.Register aaPlace = builder.addPlaceAa(operation.inputRight().type());
    builder.addOperation(Model.Op.MOV, aaPlace, operation.inputRight());
    builder.addOperation(operation.op(), operation.output(), operation.inputLeft(), aaPlace);
    builder.remove(operation);
    return builder.model();
  }
}
