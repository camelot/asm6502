package dk.camelot64.asm6502.fragment.model.pass2;

import dk.camelot64.asm6502.fragment.*;
import dk.camelot64.asm6502.fragment.model.InstructionBuilder;
import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;
import java.util.List;

/** Rewrite BIN_SIMPLE(AA, U8) to ASM. */
public class BinSimpleToAsm implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return !operation.isAsm()
        && operation.isBinSimple()
        && operation.inputLeft().isRegAa()
        && operation.output().isRegAa()
        && (operation.inputRight().isAnyMem() || operation.inputRight().isConst());
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final Model.Op op = operation.op();
    String mnemonic = Model.Op.BINARY_SIMPLE_MNEMONICS.get(op);
    final Fragment.Instruction asm =
        InstructionBuilder.instruction(mnemonic, operation.inputRight());
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    final List<Model.Place> inputs = model.getEffectiveInputs(operation);
    builder.addAsmOperation(asm, operation, inputs);
    builder.remove(operation);
    return builder.model();
  }
}
