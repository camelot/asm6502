package dk.camelot64.asm6502.fragment.model;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/** Invariants that ensure models are healthy after each rewrite. */
public class ModelInvariants {
  public static void assertModel(Model model, SpecificRewrite lastRewrite) {
    LinkedHashSet<Model.PlaceId> allReferencedPlaces = new LinkedHashSet<>();
    for (Model.Operation operation : model.operations()) {
      allReferencedPlaces.addAll(operation.getAllPlaces());
    }

    for (Model.PlaceId placeId : allReferencedPlaces) {
      final Model.Place place = model.get(placeId);
      if (place == null) {
        throw new RuntimeException(
            "Rewrite ruined model "
                + lastRewrite
                + ". Found reference to place which is not in the model "
                + placeId);
      }
    }

    // Check that all places are used as input, output or through a reference
    final List<Model.Place> unusedPlaces = new ArrayList<>(model.places());
    unusedPlaces.removeIf(place -> allReferencedPlaces.contains(place.id()));
    if (unusedPlaces.size() > 0) {
      throw new RuntimeException(
          "Rewrite ruined model "
              + lastRewrite
              + ". Found an unused place in the model "
              + unusedPlaces.get(0));
    }
  }
}
