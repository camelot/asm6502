package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Rewrite BIN(U8, AA) to BIN(AA, U8). */
public class BinSimpleSwapAaLeft implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isBinSimple()
        && operation.inputLeft().isU8()
        && operation.inputRight().isU8()
        && operation.output().isU8()
        && !operation.inputLeft().isRegAa()
        && operation.inputRight().isRegAa();
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    builder.addOperation(
        operation.op(), operation.output(), operation.inputRight(), operation.inputLeft());
    builder.remove(operation);
    return builder.model();
  }
}
