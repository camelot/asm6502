package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/**
 * Rewrite DEREF_IDX(ZP, MEM/const) to use intermediate YY MOV(YY, MEM/const), DEREF_IDX(ZP, YY).
 */
public class DerefIdxMemThroughYy implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isDerefIdx()
        && operation.inputLeft().isZeropage()
        && (operation.inputRight().isAnyMem() || operation.inputRight().isConst());
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    final Model.Place.Register yyPlace = builder.addPlaceYy(operation.inputRight().type());
    builder.addOperation(Model.Op.MOV, yyPlace, operation.inputRight());
    builder.addOperation(Model.Op.DEREF_IDX, operation.output(), operation.inputLeft(), yyPlace);
    builder.remove(operation);
    return builder.model();
  }
}
