package dk.camelot64.asm6502.fragment.model.pass2;

import dk.camelot64.asm6502.fragment.Fragment;
import dk.camelot64.asm6502.fragment.model.InstructionBuilder;
import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;
import java.util.List;

/** Rewrite MOV(REG, U8) to STA/STX/STY ASM. */
public class StoreToAsm implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return !operation.isAsm() && operation.isStore() && operation.output().isAnyMem();
    // TODO: Prevent STX abs,x, STY abs,y, STX (zp),y, STY (zp),y
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    final Model.Place.Register register = (Model.Place.Register) operation.input();
    String mnemonic = register.reg().storeMnemonic();
    final Fragment.Instruction asm = InstructionBuilder.instruction(mnemonic, operation.output());
    final ModelBuilder builder = new ModelBuilder(newModelId, model);
    final List<Model.Place> inputs = model.getEffectiveInputs(operation);
    builder.addAsmOperation(asm, operation, inputs);
    builder.remove(operation);
    return builder.model();
  }
}
