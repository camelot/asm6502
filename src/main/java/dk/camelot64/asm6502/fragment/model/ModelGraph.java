package dk.camelot64.asm6502.fragment.model;

import dk.camelot64.asm6502.fragment.GraphViz;
import java.util.function.Predicate;

/** Generates a graph for {@link Model}. */
public final class ModelGraph {

  /** Prefix added to all nodes/edges to differentiate between multiple sub-graphs. */
  private final String prefix;

  /** Model begin pretty-printed. */
  private final Model model;

  /** Model used for generating diff coloring. May be null. */
  private final Model diff;

  /** The graph being generated. */
  private final GraphViz graphViz;

  private ModelGraph(Model model, Model diff, GraphViz graphViz) {
    this.model = model;
    this.prefix = "m" + model.id();
    this.diff = diff;
    this.graphViz = graphViz;
  }

  public static void graph(GraphViz graphViz, Model model, String title, Model diff) {
    new ModelGraph(model, diff, graphViz).graph(title);
  }

  private void graph(String title) {
    graphViz.subgraphStart(prefix, title);
    graphViz.styles("    node [shape=rect];\n");
    for (Model.Place place : model.places()) {
      graph(place);
    }
    graphViz.styles("    node [shape=oval];\n");
    for (Model.Operation operation : model.operations()) {
      graph(operation);
    }
    graphViz.subgraphEnd();
  }

  void graph(Model.Place place) {
    // Generate place node
    graphViz.node(
        graphId(place),
        ModelPretty.pretty(place),
        isDiff(place),
        isAsm(place) ? "Courier New" : null,
        null);
    // Generate place relations
    if (place instanceof Model.Place.Constant constant) {
      final Model.PlaceId exprId = constant.valueExpr().placeId();
      placeRelation(place, exprId);
    } else if (place instanceof Model.Place.MemPlace memPlace) {
      final Model.PlaceId addrId = memPlace.addrExpr().placeId();
      placeRelation(place, addrId);
      if (memPlace instanceof Model.Place.AbsXy absXy) {
        final Model.PlaceId idxPlaceId = absXy.idxPlaceId();
        placeRelation(place, idxPlaceId);
      } else if (memPlace instanceof Model.Place.IndirectZpY indirectZpY) {
        final Model.PlaceId yyPlaceId = indirectZpY.yyPlaceId();
        placeRelation(place, yyPlaceId);
      }
    }
  }

  private void placeRelation(Model.Place place, Model.PlaceId relatedPlaceId) {
    if (!place.id().equals(relatedPlaceId)) {
      graphViz.edge(graphId(relatedPlaceId), graphId(place), "", "dotted", "none");
    }
  }

  private void graph(Model.Operation operation) {
    //  Generate Operation Node
    graphViz.node(
        graphId(operation),
        ModelPretty.pretty(operation),
        isDiff(operation),
        operation.isAsm() ? "Courier New" : null,
        operation.isAsm() ? "hexagon" : null);
    if (operation.inputs().size() == 2) {
      graphViz.edge(graphId(operation.inputs().get(0)), graphId(operation), "left", null, null);
      graphViz.edge(graphId(operation.inputs().get(1)), graphId(operation), "right", null, null);
    } else {
      for (Model.Place input : operation.inputs()) {
        graphViz.edge(graphId(input), graphId(operation), null, null, null);
      }
    }
    for (Model.Place output : operation.outputs()) {
      // Output Edge
      graphViz.edge(graphId(operation), graphId(output), null, null, null);
    }
  }

  private boolean isDiff(Model.Place place) {
    if (diff == null) return false;
    final Model.Place diffPlace = diff.get(place.id());
    return diffPlace == null || !diffPlace.equals(place);
  }

  private boolean isDiff(Model.Operation operation) {
    if (diff == null) return false;
    return diff.operations().stream().noneMatch(Predicate.isEqual(operation));
  }

  private static boolean isAsm(Model.Place place) {
    return (place instanceof Model.Place.MemPlace)
        || (place instanceof Model.Place.Register)
        || (place instanceof Model.Place.Constant);
  }

  private String graphId(Model.Place place) {
    final Model.PlaceId placeId = place.id();
    return graphId(placeId);
  }

  private String graphId(Model.PlaceId placeId) {
    return prefix + "_p" + placeId.name();
  }

  private String graphId(Model.Operation operation) {
    return prefix + "_o" + operation.id().id();
  }
}
