package dk.camelot64.asm6502.fragment.model;

import dk.camelot64.asm6502.fragment.Fragment;
import dk.camelot64.asm6502.fragment.FragmentToKickAss;
import dk.camelot64.asm6502.fragment.GraphViz;
import java.util.ArrayList;
import java.util.List;

/** Converts ASM {@link Model} to an {@link Fragment}. */
public class ModelToFragment {

  public static Fragment toAsm(List<Fragment.Instruction> instructions) {
    return new Fragment(new ArrayList<>(instructions));
  }

  public static void asmGraph(GraphViz graphViz, Fragment fragment) {
    graphViz.subgraphStart("asm", "asm");
    graphViz.styles("    node [shape=none];");
    final String asmCode = FragmentToKickAss.toAsm(fragment).replace("\n", "\\l");
    graphViz.node("asm", asmCode, false, "Courier New", null);
    graphViz.subgraphEnd();
  }
}
