package dk.camelot64.asm6502.fragment.model.pass1;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import dk.camelot64.asm6502.fragment.model.Rewrite;

/** Rewrite DEREF_IDX(ZP, YY) to IND_ZPY(ZP, YY). */
public class DerefIdxToZpy implements Rewrite.OperationRule {
  @Override
  public boolean match(Model.Operation operation, Model model) {
    return operation.isDerefIdx()
        && operation.inputLeft().isZeropage()
        && operation.inputRight().isRegYy()
        && operation.output().isDerefIdx();
  }

  @Override
  public Model rewrite(Model.Operation operation, Model model, int newModelId) {
    Model.Place.ZeroPage inputLeftZp = (Model.Place.ZeroPage) operation.inputLeft();
    Model.Place.Register inputRightReg = (Model.Place.Register) operation.inputRight();

    final Model.Place.IndirectZpY newOutput =
        new Model.Place.IndirectZpY(
            operation.output().id(),
            operation.output().type(),
            inputLeftZp.addrExpr(),
            inputRightReg.id());
    ModelBuilder builder = new ModelBuilder(newModelId, model);
    builder.remove(operation);
    builder.replace(operation.output(), newOutput);
    return builder.model();
  }
}
