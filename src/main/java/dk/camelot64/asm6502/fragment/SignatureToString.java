package dk.camelot64.asm6502.fragment;

public class SignatureToString {

  public static String toString(Signature signature) {
    if (signature instanceof Signature.Assignment assignment) {
      return toString(assignment.lVal()) + "=" + toString(assignment.rVal());
    }
    throw new RuntimeException("Unknown signature " + signature);
  }

  private static String toString(Signature.Expr expr) {
    if (expr instanceof Signature.Expr.Constant constant) {
      return toString(constant.type()) + "c" + constant.id();
    } else if (expr instanceof Signature.Expr.Memory memory) {
      return toString(memory.type()) + "m" + memory.id();
    } else if (expr instanceof Signature.Expr.ZeroPage zeroPage) {
      return toString(zeroPage.type()) + "z" + zeroPage.id();
    } else if (expr instanceof Signature.Expr.Register register) {
      return toString(register.type()) + register.reg().name().toLowerCase();
    } else if (expr instanceof Signature.Expr.Binary binary) {
      return toString(binary.binOp(), binary.left(), binary.right());
    } else if (expr instanceof Signature.Expr.Unary unary) {
      return toString(unary.unOp(), unary.value());
    }
    throw new RuntimeException("Unknown expression " + expr);
  }

  private static String toString(Signature.Expr.UnOp unOp, Signature.Expr value) {
    return switch (unOp) {
      case DEREF -> "*" + toString(value);
      case NEG, INC, DEC -> unOp.name().toLowerCase() + "_" + toString(value);
    };
  }

  private static String toString(
      Signature.Expr.BinOp binOp, Signature.Expr left, Signature.Expr right) {
    return switch (binOp) {
      case DEREFIDX -> toString(left) + "[" + toString(right) + "]";
      case BAND, BOR, BXOR -> toString(left)
          + "_"
          + binOp.name().toLowerCase()
          + "_"
          + toString(right);
    };
  }

  private static String toString(Signature.Type type) {
    if (type instanceof Signature.Type.Basic basic) {
      return "v" + basicToString(basic);
    } else if (type instanceof Signature.Type.Pointer pointer) {
      return "p" + basicToString((Signature.Type.Basic) pointer.toType());
    }
    throw new RuntimeException("Unknown type " + type);
  }

  private static String basicToString(Signature.Type.Basic basic) {
    return switch (basic) {
      case U8 -> "bu";
      case I8 -> "bs";
      case U16 -> "wu";
      case I16 -> "ws";
    };
  }
}
