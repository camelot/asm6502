package dk.camelot64.asm6502.fragment;

/** Prints an ASM AST in KickAssembler format for assembling. */
public class FragmentToKickAss {

  private StringBuilder output;

  private FragmentToKickAss() {
    this.output = new StringBuilder();
  }

  private StringBuilder getOutput() {
    return output;
  }

  public static String toAsm(Fragment asm) {
    final FragmentToKickAss asmGen = new FragmentToKickAss();
    asm.body().forEach(asmGen::toAsm);
    return asmGen.getOutput().toString();
  }

  private void toAsm(Fragment.Element asm) {
    if (asm instanceof Fragment.Instruction instr) {
      output.append(toAsm(instr)).append("\n");
    } else if (asm instanceof Fragment.Label label) {
      output.append(label.name()).append(": ");
    } else {
      throw new RuntimeException("Not supported " + asm);
    }
  }

  public static String toAsm(Fragment.Instruction instr) {
    Fragment.AddressingMode addressingMode = instr.mode();
    record InstructionFormat(String format, int numOperands) {}
    final InstructionFormat format =
        switch (addressingMode) {
          case NON -> new InstructionFormat("%s", 0);
          case IMM -> new InstructionFormat("%s #%s", 1);
          case ABS -> new InstructionFormat("%s %s", 1);
          case ABS2 -> new InstructionFormat("%s %s,%s", 2);
          case ABS3 -> new InstructionFormat("%s %s,%s,%s", 3);
          case ABS_X -> new InstructionFormat("%s %s,x", 1);
          case ABS_Y -> new InstructionFormat("%s %s,y", 1);
          case IMM_ABS -> new InstructionFormat("%s #%s,%s", 2);
          case IMM_ABS_X -> new InstructionFormat("%s #%s,%s,x", 2);
          case IND -> new InstructionFormat("%s (%s)", 1);
          case LIND -> new InstructionFormat("%s ((%s))", 1);
          case IND_X -> new InstructionFormat("%s (%s,x)", 1);
          case IND_Y -> new InstructionFormat("%s (%s),y", 1);
          case IND_Z -> new InstructionFormat("%s (%s),z", 1);
          case LIND_Z -> new InstructionFormat("%s ((%s)),z", 1);
          case SPIND_Y -> new InstructionFormat("%s (%s,sp),y", 1);
          default -> throw new RuntimeException("Not supported " + addressingMode);
        };
    final String formatted =
        switch (format.numOperands) {
          case 0 -> format.format.formatted(instr.mnemonic());
          case 1 -> format.format.formatted(instr.mnemonic(), toAsm(instr.operand1()));
          case 2 -> format.format.formatted(
              instr.mnemonic(), toAsm(instr.operand1()), toAsm(instr.operand2()));
          case 3 -> format.format.formatted(
              instr.mnemonic(),
              toAsm(instr.operand1()),
              toAsm(instr.operand2()),
              toAsm(instr.operand3()));
          default -> throw new RuntimeException("Not supported " + format);
        };
    return formatted;
  }

  public static String toAsm(Fragment.Expr expr) {
    if (expr instanceof Fragment.Expr.Int exprInt) {
      return Integer.toString(exprInt.value());
    } else if (expr instanceof Fragment.Expr.Binary exprBin) {
      return "["
          + toAsm(exprBin.left())
          + exprBin.operator().operator
          + toAsm(exprBin.right())
          + "]";
    } else if (expr instanceof Fragment.Expr.Unary exprUn) {
      return "[" + exprUn.operator().operator + toAsm(exprUn.sub()) + "]";
    } else if (expr instanceof Fragment.Expr.Replace replace) {
      return "{" + replace.name() + "}";
    } else if (expr instanceof Fragment.Expr.Identifier identifier) {
      return identifier.name();
    } else {
      throw new RuntimeException("Not supported " + expr);
    }
  }
}
