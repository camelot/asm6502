package dk.camelot64.asm6502.fragment;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelGraph;
import dk.camelot64.asm6502.fragment.model.ModelToFragment;

/** Creates a GraphViz graph showing the stages of model conversion and rewriting. */
public class ForgerGraph implements ForgerDebug {

  GraphViz graphViz;

  public ForgerGraph() {
    graphViz = new GraphViz();
    graphViz.header();
  }

  @Override
  public void signature(Signature signature) {
    SignatureGraph.graph(graphViz, signature, "sig");
  }

  @Override
  public void model(Model model, String title, Model diff) {
    ModelGraph.graph(graphViz, model, model.id() + ":" + title, diff);
  }

  @Override
  public void asm(Fragment fragment) {
    ModelToFragment.asmGraph(graphViz, fragment);
  }

  public String getGraph() {
    graphViz.footer();
    return graphViz.getGraph();
  }
}
