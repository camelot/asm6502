package dk.camelot64.asm6502.fragment;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelBuilder;
import java.util.Map;

/** Converts ASM fragment signatures to a fragment model. */
public final class SignatureToModel {

  public static Model convert(Signature signature, int modelId) {
    final ModelBuilder builder = new ModelBuilder(modelId, makeAllocator(signature));
    if (signature instanceof Signature.Assignment assignment) {
      builder.addOperation(
          Model.Op.MOV,
          convert(assignment.lVal(), true, builder),
          convert(assignment.rVal(), false, builder));
      return builder.model();
    } else {
      throw new RuntimeException("Unknown signature " + signature);
    }
  }

  private static ModelBuilder.IdAllocator makeAllocator(Signature signature) {
    if (signature instanceof Signature.Assignment assignment) {
      return ModelBuilder.IdAllocator.max(
          makeAllocator(assignment.lVal()), makeAllocator(assignment.rVal()));
    } else {
      throw new RuntimeException("Unknown signature " + signature);
    }
  }

  private static ModelBuilder.IdAllocator makeAllocator(Signature.Expr expr) {
    if (expr instanceof Signature.Expr.ZeroPage zeroPage) {
      return new ModelBuilder.IdAllocator(zeroPage.id() + 1, 1, 1, 1, 1);
    } else if (expr instanceof Signature.Expr.Memory memory) {
      return new ModelBuilder.IdAllocator(memory.id() + 1, 1, 1, 1, 1);
    } else if (expr instanceof Signature.Expr.Constant constant) {
      return new ModelBuilder.IdAllocator(1, constant.id() + 1, 1, 1, 1);
    } else if (expr instanceof Signature.Expr.Register) {
      return new ModelBuilder.IdAllocator(1, 1, 1, 1, 1);
    } else if (expr instanceof Signature.Expr.Binary binary) {
      return ModelBuilder.IdAllocator.max(
          makeAllocator(binary.left()), makeAllocator(binary.right()));
    } else if (expr instanceof Signature.Expr.Unary unary) {
      return makeAllocator(unary.value());
    } else {
      throw new RuntimeException("Unknown signature " + expr);
    }
  }

  private static Model.Place convert(Signature.Expr expr, boolean lValue, ModelBuilder builder) {
    if (expr instanceof Signature.Expr.Register register) {
      // For non-lvalues Register ID's must be re-used
      final Model.Place.Reg reg = convert(register.reg());
      if (!lValue) {
        final Model.Place registerPlace = builder.findRegisterRVal(reg);
        if (registerPlace != null) {
          return registerPlace;
        }
      }
      // Not an RValue or RValue not found
      final Model.PlaceId registerId = builder.newId(Model.PlaceKind.REGISTER, lValue);
      return builder.add(new Model.Place.Register(registerId, convert(register.type()), reg));
    } else if (expr instanceof Signature.Expr.Constant constant) {
      final Model.PlaceId constId =
          Model.PlaceId.of(Model.PlaceKind.CONSTANT, constant.id(), lValue);
      return builder.add(
          new Model.Place.Constant(constId, convert(constant.type()), Model.PlaceExpr.id(constId)));
    } else if (expr instanceof Signature.Expr.Memory memory) {
      final Model.PlaceId memId = Model.PlaceId.of(Model.PlaceKind.MAINMEMORY, memory.id(), lValue);
      return builder.add(
          new Model.Place.MainMemory(memId, convert(memory.type()), Model.PlaceExpr.id(memId)));
    } else if (expr instanceof Signature.Expr.ZeroPage zeroPage) {
      final Model.PlaceId zpId = Model.PlaceId.of(Model.PlaceKind.ZEROPAGE, zeroPage.id(), lValue);
      return builder.add(
          new Model.Place.ZeroPage(zpId, convert(zeroPage.type()), Model.PlaceExpr.id(zpId)));
    } else if (expr instanceof Signature.Expr.Binary binary) {
      final Model.Place left = convert(binary.left(), false, builder);
      final Model.Place right = convert(binary.right(), false, builder);
      if (Signature.Expr.BinOp.DEREFIDX.equals(binary.binOp())) {
        final Model.Type.Pointer ptrType = (Model.Type.Pointer) left.type();
        final Model.Place derefIdxPlace =
            builder.add(
                new Model.Place.DerefIdx(
                    builder.newId(Model.PlaceKind.OTHER, lValue), ptrType.toType()));
        builder.addOperation(Model.Op.DEREF_IDX, derefIdxPlace, left, right);
        return derefIdxPlace;
      } else if (BINOP_MAP.containsKey(binary.binOp())) {
        final Model.Type type = left.type();
        if (!right.type().equals(type))
          throw new RuntimeException("Non-matching types for binary operator " + binary);
        final Model.Place resultPlace;
        if (Model.Type.Basic.U8.equals(type)) {
          resultPlace = builder.addPlaceAa(type);
        } else {
          resultPlace = builder.addPlaceAny(type);
        }
        builder.addOperation(BINOP_MAP.get(binary.binOp()), resultPlace, left, right);
        return resultPlace;
      }
    } else if (expr instanceof Signature.Expr.Unary unary) {
      final Model.Place value = convert(unary.value(), false, builder);
      if (Signature.Expr.UnOp.DEREF.equals(unary.unOp())) {
        final Model.Type.Pointer ptrType = (Model.Type.Pointer) value.type();
        final Model.Place derefPlace =
            builder.add(
                new Model.Place.Deref(
                    builder.newId(Model.PlaceKind.OTHER, lValue), ptrType.toType()));
        builder.addOperation(Model.Op.DEREF, derefPlace, value);
        return derefPlace;
      } else if (UNOP_MAP.containsKey(unary.unOp())) {
        if (!value.isReg()) throw new RuntimeException("Only supports registers " + unary);
        final Model.Place resultPlace =
            builder.add(
                new Model.Place.Register(
                    builder.newId(Model.PlaceKind.REGISTER, false), value.type(), value.reg()));
        builder.addOperation(UNOP_MAP.get(unary.unOp()), resultPlace, value);
        return resultPlace;
      }
    }
    throw new RuntimeException("Unknown signature expression " + expr.toString());
  }

  static final Map<Signature.Expr.UnOp, Model.Op> UNOP_MAP =
      Map.of(
          Signature.Expr.UnOp.INC, Model.Op.INC,
          Signature.Expr.UnOp.DEC, Model.Op.DEC);

  static final Map<Signature.Expr.BinOp, Model.Op> BINOP_MAP =
      Map.of(
          Signature.Expr.BinOp.BOR, Model.Op.BIN_OR,
          Signature.Expr.BinOp.BXOR, Model.Op.BIN_XOR,
          Signature.Expr.BinOp.BAND, Model.Op.BIN_AND);

  static final Map<Signature.Type.Basic, Model.Type.Basic> BASIC_TYPE_MAP =
      Map.of(
          Signature.Type.Basic.U8, Model.Type.Basic.U8,
          Signature.Type.Basic.I8, Model.Type.Basic.I8,
          Signature.Type.Basic.U16, Model.Type.Basic.U16,
          Signature.Type.Basic.I16, Model.Type.Basic.I16);

  private static Model.Type convert(Signature.Type sigType) {
    if (sigType instanceof Signature.Type.Basic sigBasic) {
      return BASIC_TYPE_MAP.get(sigBasic);
    } else if (sigType instanceof Signature.Type.Pointer sigPointer) {
      return Model.Type.Pointer.to(convert(sigPointer.toType()));
    } else {
      throw new RuntimeException("Unknown signature type" + sigType.toString());
    }
  }

  public static final Map<Signature.Expr.Reg, Model.Place.Reg> REGISTER_MAP =
      Map.of(
          Signature.Expr.Reg.AA, Model.Place.Reg.AA,
          Signature.Expr.Reg.XX, Model.Place.Reg.XX,
          Signature.Expr.Reg.YY, Model.Place.Reg.YY,
          Signature.Expr.Reg.ZZ, Model.Place.Reg.ZZ);

  private static Model.Place.Reg convert(Signature.Expr.Reg sigReg) {
    return REGISTER_MAP.get(sigReg);
  }
}
