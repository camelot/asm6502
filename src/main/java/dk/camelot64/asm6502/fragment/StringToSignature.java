package dk.camelot64.asm6502.fragment;

import dk.camelot64.asm6502.fragment.parser.SignatureLexer;
import dk.camelot64.asm6502.fragment.parser.SignatureParser;
import org.antlr.v4.runtime.*;

public class StringToSignature {

  public static Signature parse(String signature) {
    final CharStream charStream = CharStreams.fromString(signature, "_inline_");
    final SignatureLexer lexer = new SignatureLexer(charStream);
    final TokenStream tokenStream = new CommonTokenStream(lexer);
    final SignatureParser parser = new SignatureParser(tokenStream);
    parser.setErrorHandler(new BailErrorStrategy());
    final SignatureParser.SignatureContext signatureContext = parser.signature();
    final StringToSignature toSignature = new StringToSignature();
    return toSignature.convert(signatureContext);
  }

  private Signature convert(SignatureParser.SignatureContext signatureCtx) {
    if (signatureCtx instanceof SignatureParser.AssignmentContext assignmentCtx) {
      return new Signature.Assignment(
          convert(assignmentCtx.expr(0)), convert(assignmentCtx.expr(1)));
    } else {
      throw new RuntimeException("Unknown signature " + signatureCtx.getText());
    }
  }

  private Signature.Expr convert(SignatureParser.ExprContext exprCtx) {
    if (exprCtx instanceof SignatureParser.ParContext par) {
      return convert(par.expr());
    } else if (exprCtx instanceof SignatureParser.BinaryContext binary) {
      final String operator = binary.children.get(1).getText();
      final Signature.Expr.BinOp binOp =
          switch (operator) {
            case "band":
              yield Signature.Expr.BinOp.BAND;
            case "bor":
              yield Signature.Expr.BinOp.BOR;
            case "bxor":
              yield Signature.Expr.BinOp.BXOR;
            case "derefidx":
              yield Signature.Expr.BinOp.DEREFIDX;
            default:
              throw new RuntimeException("Unknown binary operator " + operator);
          };
      return new Signature.Expr.Binary(binOp, convert(binary.expr(0)), convert(binary.expr(1)));
    } else if (exprCtx instanceof SignatureParser.UnaryContext unary) {
      final String operator = unary.children.get(0).getText();
      final Signature.Expr.UnOp unOp =
          switch (operator) {
            case "inc":
              yield Signature.Expr.UnOp.INC;
            case "dec":
              yield Signature.Expr.UnOp.DEC;
            default:
              throw new RuntimeException("Unknown unary operator " + operator);
          };
      return new Signature.Expr.Unary(unOp, convert(unary.expr()));
    } else if (exprCtx instanceof SignatureParser.DerefSimpleContext deref) {
      return new Signature.Expr.Unary(Signature.Expr.UnOp.DEREF, convert(deref.expr()));
    } else if (exprCtx instanceof SignatureParser.DerefIdxContext derefIdx) {
      return new Signature.Expr.Binary(
          Signature.Expr.BinOp.DEREFIDX, convert(derefIdx.expr(0)), convert(derefIdx.expr(1)));
    } else if (exprCtx instanceof SignatureParser.VarContext var) {
      final String varText = var.VAR().getText();
      Signature.Type type = type(varText.substring(0, 3));
      final String varReg = varText.substring(3, 5);
      switch (varReg) {
        case "aa":
          return new Signature.Expr.Register(Signature.Expr.Reg.AA, type);
        case "xx":
          return new Signature.Expr.Register(Signature.Expr.Reg.XX, type);
        case "yy":
          return new Signature.Expr.Register(Signature.Expr.Reg.YY, type);
        case "zz":
          return new Signature.Expr.Register(Signature.Expr.Reg.ZZ, type);
      }
      final String varKind = varText.substring(3, 4);
      final int varId = Integer.parseInt(varText.substring(4, 5));
      switch (varKind) {
        case "z":
          return new Signature.Expr.ZeroPage(varId, type);
        case "m":
          return new Signature.Expr.Memory(varId, type);
        case "c":
          return new Signature.Expr.Constant(varId, type);
      }
    }
    throw new RuntimeException("Unknown signature expression " + exprCtx);
  }

  private Signature.Type type(String typeStr) {
    switch (typeStr) {
      case "vbu":
        return Signature.Type.Basic.U8;
      case "vbs":
        return Signature.Type.Basic.I8;
      case "vwu":
        return Signature.Type.Basic.U16;
      case "vws":
        return Signature.Type.Basic.I16;
      case "pbu":
        return Signature.Type.Pointer.to(Signature.Type.Basic.U8);
      case "pbs":
        return Signature.Type.Pointer.to(Signature.Type.Basic.I8);
      case "pwu":
        return Signature.Type.Pointer.to(Signature.Type.Basic.U16);
      case "pws":
        return Signature.Type.Pointer.to(Signature.Type.Basic.I16);
    }
    throw new RuntimeException("Unknown signature type " + typeStr);
  }
}
