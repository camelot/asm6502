package dk.camelot64.asm6502.fragment;

/** Pretty-prints signatures. */
public class SignaturePretty {

  public static String pretty(Signature signature) {
    if (signature instanceof Signature.Assignment assignment) {
      return pretty(assignment.lVal()) + "=" + pretty(assignment.rVal());
    } else {
      throw new RuntimeException("Unknown signature " + signature);
    }
  }

  private static String pretty(Signature.Expr expr) {
    if (expr instanceof Signature.Expr.Register register) {
      return pretty(register.type()) + pretty(register.reg());
    } else if (expr instanceof Signature.Expr.Constant constant) {
      return pretty(constant.type()) + "c" + constant.id();
    } else if (expr instanceof Signature.Expr.ZeroPage zeroPage) {
      return pretty(zeroPage.type()) + "z" + zeroPage.id();
    } else if (expr instanceof Signature.Expr.Memory memory) {
      return pretty(memory.type()) + "m" + memory.id();
    } else if (expr instanceof Signature.Expr.Unary unary) {
      final Signature.Expr.UnOp unOp = unary.unOp();
      if (Signature.Expr.UnOp.DEREF.equals(unOp)) {
        return "*" + pretty(unary.value());
      } else {
        return unOp.name().toLowerCase() + "_" + pretty(unary.value());
      }
    } else if (expr instanceof Signature.Expr.Binary binary) {
      final Signature.Expr.BinOp binOp = binary.binOp();
      if (Signature.Expr.BinOp.DEREFIDX.equals(binOp)) {
        return pretty(binary.left()) + "[" + pretty(binary.right()) + "]";
      } else {
        return pretty(binary.left())
            + "_"
            + binOp.name().toLowerCase()
            + "_"
            + pretty(binary.right());
      }
    } else {
      throw new RuntimeException("Unknown signature expression " + expr);
    }
  }

  private static String pretty(Signature.Expr.Reg reg) {
    return reg.name().toLowerCase();
  }

  private static String pretty(Signature.Type type) {
    if (type instanceof Signature.Type.Basic basic) {
      return "v" + baseType(basic);
    } else if (type instanceof Signature.Type.Pointer pointer) {
      return "p" + baseType((Signature.Type.Basic) pointer.toType());
    } else {
      throw new RuntimeException("Unknown signature type " + type);
    }
  }

  private static String baseType(Signature.Type.Basic basic) {
    return switch (basic) {
      case U8 -> "bu";
      case I8 -> "bs";
      case U16 -> "wu";
      case I16 -> "ws";
    };
  }
}
