package dk.camelot64.asm6502.fragment;

import java.util.Arrays;
import java.util.List;

/** An 65xx assembler program */
public record Fragment(List<Element> body) {

  /**
   * Unresolved Addressing Mode. The addressing mode can only be resolved after parsing and
   * analyzing the program. Until enough information is available an unresolved addressing mode is
   * used.
   */
  public enum AddressingMode {
    /** None/Implied. */
    NON,
    /** #imm Immediate (byte or word). */
    IMM,
    /** abs Absolute (absolute or zero page). */
    ABS,
    /** abs,abs Absolute *2 (absolute or zero page). */
    ABS2,
    /** abs,abs,abs Absolute *3 (always absolute). */
    ABS3,
    /** addr,x X-indexed (always absolute). */
    ABS_X,
    /** addr,y Y-indexed (absolute or zero page). */
    ABS_Y,
    /** (addr) Indirect (absolute or zero page). */
    IND,
    /** (addr,x) Indirect X-indexed (absolute or zero page). */
    IND_X,
    /** (addr),y Indirect Y-indexed (absolute or zero page). */
    IND_Y,
    /** (addr),z Indirect Z-indexed (absolute or zero page). */
    IND_Z,
    /** ((addr)) 32-bit Indirect (always zero page). */
    LIND,
    /** ((addr)),z 32-bit Indirect Z-indexed (always zero page). */
    LIND_Z,
    /** (addr,sp),y Stack Pointer Indirect Indexed (always zero page). */
    SPIND_Y,
    /** #imm, abs Immediate, absolute. */
    IMM_ABS,
    /** #imm, abs, x Immediate, absolute X-indexed. */
    IMM_ABS_X,
  }

  public sealed interface Expr
      permits Expr.Replace, Expr.Identifier, Expr.Int, Expr.Chr, Expr.Str, Expr.Binary, Expr.Unary {

    record Replace(String name) implements Expr {}

    record Identifier(String name) implements Expr {}

    record Int(int value) implements Expr {}

    record Chr(char value) implements Expr {}

    record Str(String value) implements Expr {}

    enum BinOp {
      DOT("."),
      MULTIPLY("*"),
      DIVIDE("/"),
      REMAINDER("%"),
      PLUS("+"),
      MINUS("-"),
      SHIFT_LEFT("<<"),
      SHIFT_RIGHT(">>"),
      BIT_AND("&"),
      BIT_OR("|"),
      BIT_XOR("^"),
      LOGIC_AND("&&"),
      LOGIC_OR("||"),
      LT("<"),
      GT(">"),
      LE("<="),
      GE(">="),
      EQ("=="),
      NEQ("!=");

      public final String operator;

      BinOp(String operator) {
        this.operator = operator;
      }

      public static BinOp fromOp(String operator) {
        return Arrays.stream(BinOp.values())
            .filter(binOp -> binOp.operator.equalsIgnoreCase(operator))
            .findFirst()
            .orElseThrow(RuntimeException::new);
      }
    }

    record Binary(Expr.BinOp operator, Expr left, Expr right) implements Expr {}

    enum UnOp {
      NEG("-"),
      LOGIC_NOT("!"),
      BIT_NOT("~"),
      LOW("<"),
      HIGH(">");

      public final String operator;

      UnOp(String operator) {
        this.operator = operator;
      }

      public static UnOp fromOp(String operator) {
        return Arrays.stream(UnOp.values())
            .filter(op -> op.operator.equalsIgnoreCase(operator))
            .findFirst()
            .orElseThrow(RuntimeException::new);
      }
    }

    record Unary(Expr.UnOp operator, Expr sub) implements Expr {}
  }

  /** An element that is part of an ASM program. */
  public sealed interface Element permits Instruction, Cpu, Scope, Block, Label, Bytes {}

  public record Instruction(
      String mnemonic, AddressingMode mode, Expr operand1, Expr operand2, Expr operand3)
      implements Element {

    /** Special instruction representing "no instruction" */
    public static final Fragment.Instruction NONE = new Fragment.Instruction("");

    public Instruction(String mnemonic) {
      this(mnemonic, AddressingMode.NON, null, null, null);
    }

    public Instruction(String mnemonic, AddressingMode mode, Expr operand1) {
      this(mnemonic, mode, operand1, null, null);
    }

    @Override
    public String toString() {
      return FragmentToKickAss.toAsm(this);
    }
  }

  public record Cpu(String cpuId) implements Element {}

  public record Scope(String name, List<Element> body) implements Element {}

  public record Block(List<Element> body) implements Element {}

  public record Label(String name) implements Element {}

  public record Bytes(List<Expr> exprs) implements Element {}
}
