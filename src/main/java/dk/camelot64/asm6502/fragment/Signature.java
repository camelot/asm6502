package dk.camelot64.asm6502.fragment;

/**
 * A ASM fragment signature.
 *
 * <p>An example is the signature <code>pbuz1=pbuz1+pbuc1[vbuxx]</code>.
 */
public sealed interface Signature permits Signature.Assignment {

  record Assignment(Expr lVal, Expr rVal) implements Signature {}

  /** An expression that is part of an ASM signature. */
  sealed interface Expr
      permits Expr.Binary, Expr.Unary, Expr.Register, Expr.Memory, Expr.ZeroPage, Expr.Constant {

    enum BinOp {
      BAND,
      BOR,
      BXOR,
      DEREFIDX,
    }

    record Binary(BinOp binOp, Expr left, Expr right) implements Expr {}

    enum UnOp {
      NEG,
      INC,
      DEC,
      DEREF,
    }

    record Unary(UnOp unOp, Expr value) implements Expr {}

    enum Reg {
      AA,
      XX,
      YY,
      ZZ,
    }

    record Register(Reg reg, Type type) implements Expr {}

    record Memory(int id, Type type) implements Expr {}

    record ZeroPage(int id, Type type) implements Expr {}

    record Constant(int id, Type type) implements Expr {}
  }

  /** Type of value (constant, in register or in memory) that is part of an ASM signature. */
  sealed interface Type permits Type.Basic, Type.Pointer {

    enum Basic implements Type {
      I8,
      U8,
      I16,
      U16
    }

    record Pointer(Type toType) implements Type {

      public static Type to(Type toType) {
        return new Pointer(toType);
      }
    }
  }
}
