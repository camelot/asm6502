package dk.camelot64.asm6502.fragment;

import dk.camelot64.asm6502.fragment.model.Model;

/** Supports collecting/emitting information about a model during fragment forging. */
public interface ForgerDebug {

  void signature(Signature signature);

  void model(Model model, String roundTitle, Model diff);

  void asm(Fragment fragment);

  public static ForgerDebug NONE =
      new ForgerDebug() {
        @Override
        public void signature(Signature signature) {}

        @Override
        public void model(Model model, String roundTitle, Model diff) {}

        @Override
        public void asm(Fragment fragment) {}
      };
}
