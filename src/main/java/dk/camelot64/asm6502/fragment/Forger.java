package dk.camelot64.asm6502.fragment;

import dk.camelot64.asm6502.fragment.model.*;
import java.util.List;

public class Forger {

  public static Fragment forge(Signature signature, ForgerDebug debug) {
    debug.signature(signature);
    final IdAllocator idAllocator = new IdAllocator();
    Model initialModel = SignatureToModel.convert(signature, idAllocator.nextId());
    // Type-check the model. Does inputs and outputs match the restrictions of the operations.
    // TODO: Type Check!
    // Rewrite the model. Lower the abstraction level closer to ASM.
    final List<Model> lowModels = Pass1Lower.rewrite(initialModel, idAllocator, debug);
    Fragment bestAsm = null;
    for (Model lowModel : lowModels) {
      // Rewrite each low model to an ASM model
      final Model asmModel = Pass2Asm.rewriteToAsm(lowModel, idAllocator);
      // If the model was rewritten to ASM
      if (asmModel.isAsm()) {
        // Debug the model
        debug.model(asmModel, "ASM" + " (" + lowModel.id() + ")", initialModel);
        // Linearize the model. Ensure the operation sequence is sound.
        final List<Fragment.Instruction> instructions = ModelFlatten.flatten(asmModel);
        // Convert the instructions to a fragment
        final Fragment asm = ModelToFragment.toAsm(instructions);
        // Update the best fragment
        if (bestAsm == null || getAsmScore(asm) < getAsmScore(bestAsm)) {
          bestAsm = asm;
        }
      } else if (asmModel.id() != lowModel.id()) {
        debug.model(asmModel, "NO ASM" + " (" + lowModel.id() + ")", lowModel);
      }
    }
    if (bestAsm == null) {
      throw new RuntimeException("No ASM forged!");
    }
    debug.asm(bestAsm);
    return bestAsm;
  }

  /**
   * Get a score used to determine which ASM fragment is best.
   *
   * @param asm The ASM fragment
   * @return The score.
   */
  private static int getAsmScore(Fragment asm) {
    return asm.body().size();
  }

  /** Allocator for Model ID's */
  public static class IdAllocator {
    private int nextId;

    public IdAllocator() {
      this.nextId = 0;
    }

    public int nextId() {
      return ++nextId;
    }
  }
}
