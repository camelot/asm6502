package dk.camelot64.asm6502.fragment;

/** Generates a graph for {@link Signature}. */
public class SignatureGraph {

  /** The signature. */
  private final Signature signature;
  /** Prefix added to all nodes/edges to differentiate between multiple sub-graphs. */
  private final String prefix;

  /** The graph being generated. */
  private final GraphViz graphViz;

  private int nextId;

  private SignatureGraph(Signature signature, String prefix, GraphViz graphViz) {
    this.signature = signature;
    this.prefix = prefix;
    this.graphViz = graphViz;
    this.nextId = 1;
  }

  public static void graph(GraphViz graphViz, Signature signature, String prefix) {
    new SignatureGraph(signature, prefix, graphViz).graph();
  }

  private void graph() {
    String title = SignaturePretty.pretty(signature);
    graphViz.subgraphStart(prefix, title);
    if (signature instanceof Signature.Assignment assignment) {
      String lValId = graph(assignment.lVal());
      String rValId = graph(assignment.rVal());
      String assignId = newPrettyId();
      graphViz.node(
          assignId, signature.getClass().getSimpleName().toLowerCase(), false, null, null);
      graphViz.edge(assignId, lValId, "left", null, null);
      graphViz.edge(assignId, rValId, "right", null, null);
    } else {
      throw new RuntimeException("Unknown signature " + signature);
    }
    graphViz.subgraphEnd();
  }

  private String graph(Signature.Expr expr) {
    if (expr instanceof Signature.Expr.Constant constant) {
      final String prettyId = newPrettyId();
      String label = "@c" + constant.id() + ": " + pretty(constant.type()) + " const";
      graphViz.node(prettyId, label, false, "Courier New", "rect");
      return prettyId;
    } else if (expr instanceof Signature.Expr.Register register) {
      final String prettyId = newPrettyId();
      String label = " " + pretty(register.type()) + " " + register.reg().name().toLowerCase();
      graphViz.node(prettyId, label, false, "Courier New", "rect");
      return prettyId;
    } else if (expr instanceof Signature.Expr.Memory memory) {
      final String prettyId = newPrettyId();
      String label = "@m" + memory.id() + ": " + pretty(memory.type()) + " mem";
      graphViz.node(prettyId, label, false, "Courier New", "rect");
      return prettyId;
    } else if (expr instanceof Signature.Expr.ZeroPage zp) {
      final String prettyId = newPrettyId();
      String label = "@z" + zp.id() + ": " + pretty(zp.type()) + " zp";
      graphViz.node(prettyId, label, false, "Courier New", "rect");
      return prettyId;
    } else if (expr instanceof Signature.Expr.Unary unary) {
      final String prettyId = newPrettyId();
      String label = unary.unOp().name().toLowerCase();
      graphViz.node(prettyId, label, false, null, null);
      final String subPrettyId = graph(unary.value());
      graphViz.edge(prettyId, subPrettyId, null, null, null);
      return prettyId;
    } else if (expr instanceof Signature.Expr.Binary binary) {
      final String prettyId = newPrettyId();
      String label = binary.binOp().name().toLowerCase();
      graphViz.node(prettyId, label, false, null, null);
      final String leftPrettyId = graph(binary.left());
      graphViz.edge(prettyId, leftPrettyId, "left", null, null);
      final String rightPrettyId = graph(binary.right());
      graphViz.edge(prettyId, rightPrettyId, "right", null, null);
      return prettyId;
    } else {
      throw new RuntimeException("Unknown expr " + expr);
    }
  }

  private String pretty(Signature.Type type) {
    if (type instanceof Signature.Type.Basic basic) {
      return basic.name().toLowerCase();
    } else if (type instanceof Signature.Type.Pointer pointer) {
      return pretty(pointer.toType()) + "*";
    } else {
      throw new RuntimeException("Unknown type " + type);
    }
  }

  private String newPrettyId() {
    return prefix + "_" + nextId++;
  }
}
