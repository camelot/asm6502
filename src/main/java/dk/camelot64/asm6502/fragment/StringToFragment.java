package dk.camelot64.asm6502.fragment;

import dk.camelot64.asm6502.fragment.parser.FragmentLexer;
import dk.camelot64.asm6502.fragment.parser.FragmentParser;
import java.util.List;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;

/** Converts a parsed {@link FragmentParser.AsmFileContext} to an ASM {@link Fragment}. */
public class StringToFragment {

  public static Fragment parse(String asmFragment) {
    final CharStream charStream = CharStreams.fromString(asmFragment, "_inline_");
    final FragmentLexer lexer = new FragmentLexer(charStream);
    final TokenStream tokenStream = new CommonTokenStream(lexer);
    final FragmentParser parser = new FragmentParser(tokenStream);
    final FragmentParser.AsmFileContext asmFileCtx = parser.asmFile();
    final StringToFragment asmFragmentParser = new StringToFragment();
    return asmFragmentParser.convert(asmFileCtx);
  }

  private Fragment convert(FragmentParser.AsmFileContext ctx) {
    final List<Fragment.Element> elements =
        ctx.asmLines().asmLine().stream().map(this::convert).toList();
    return new Fragment(elements);
  }

  private Fragment.Element convert(FragmentParser.AsmLineContext ctx) {
    if (ctx.asmDirective() != null) {
      return convert(ctx.asmDirective());
    } else if (ctx.asmLabel() != null) {
      return convert(ctx.asmLabel());
    } else if (ctx.asmBytes() != null) {
      return new Fragment.Bytes(ctx.asmBytes().asmExpr().stream().map(this::convert).toList());
    } else if (ctx.asmInstruction() != null) {
      return convert(ctx.asmInstruction());
    } else {
      throw new RuntimeException("Unexpected value: " + ctx.getText());
    }
  }

  private Fragment.Instruction convert(FragmentParser.AsmInstructionContext ctx) {
    final String mnemonic = ctx.ASM_MNEMONIC().getText();
    final FragmentParser.AsmParamModeContext mode = ctx.asmParamMode();
    if (mode == null) {
      return new Fragment.Instruction(mnemonic, Fragment.AddressingMode.NON, null, null, null);
    } else if (mode instanceof FragmentParser.AsmModeImmContext instr) {
      return new Fragment.Instruction(
          mnemonic, Fragment.AddressingMode.IMM, convert(instr.asmExpr()), null, null);
    } else if (mode instanceof FragmentParser.AsmModeImmAndAbsContext instr) {
      return new Fragment.Instruction(
          mnemonic,
          Fragment.AddressingMode.IMM_ABS,
          convert(instr.asmExpr(0)),
          convert(instr.asmExpr(1)),
          null);
    } else if (mode instanceof FragmentParser.AsmModeAbsContext instr) {
      return new Fragment.Instruction(
          mnemonic, Fragment.AddressingMode.ABS, convert(instr.asmExpr()), null, null);
    } else if (mode instanceof FragmentParser.AsmModeAbs2Context instr) {
      return new Fragment.Instruction(
          mnemonic,
          Fragment.AddressingMode.ABS2,
          convert(instr.asmExpr(0)),
          convert(instr.asmExpr(1)),
          null);
    } else if (mode instanceof FragmentParser.AsmModeAbs3Context instr) {
      return new Fragment.Instruction(
          mnemonic,
          Fragment.AddressingMode.ABS3,
          convert(instr.asmExpr(0)),
          convert(instr.asmExpr(1)),
          convert(instr.asmExpr(2)));
    } else if (mode instanceof FragmentParser.AsmModeAbsXYContext instr) {
      if (instr.ASM_NAME().getText().equalsIgnoreCase("x")) {
        return new Fragment.Instruction(
            mnemonic, Fragment.AddressingMode.ABS_X, convert(instr.asmExpr()), null, null);
      } else if (instr.ASM_NAME().getText().equalsIgnoreCase("y")) {
        return new Fragment.Instruction(
            mnemonic, Fragment.AddressingMode.ABS_Y, convert(instr.asmExpr()), null, null);
      }
    } else if (mode instanceof FragmentParser.AsmModeIndContext instr) {
      return new Fragment.Instruction(
          mnemonic, Fragment.AddressingMode.IND, convert(instr.asmExpr()), null, null);
    } else if (mode instanceof FragmentParser.AsmModeIndLongContext instr) {
      return new Fragment.Instruction(
          mnemonic, Fragment.AddressingMode.LIND, convert(instr.asmExpr()), null, null);
    } else if (mode instanceof FragmentParser.AsmModeIndIdxYZContext instr) {
      if (instr.ASM_NAME().getText().equalsIgnoreCase("y")) {
        return new Fragment.Instruction(
            mnemonic, Fragment.AddressingMode.IND_Y, convert(instr.asmExpr()), null, null);
      } else if (instr.ASM_NAME().getText().equalsIgnoreCase("z")) {
        return new Fragment.Instruction(
            mnemonic, Fragment.AddressingMode.IND_Z, convert(instr.asmExpr()), null, null);
      }
    } else if (mode instanceof FragmentParser.AsmModeIdxIndXContext instr) {
      if (instr.ASM_NAME().getText().equalsIgnoreCase("x")) {
        return new Fragment.Instruction(
            mnemonic, Fragment.AddressingMode.IND_X, convert(instr.asmExpr()), null, null);
      }
    } else if (mode instanceof FragmentParser.AsmModeIndLongIdxZContext instr) {
      if (instr.ASM_NAME().getText().equalsIgnoreCase("z")) {
        return new Fragment.Instruction(
            mnemonic, Fragment.AddressingMode.LIND_Z, convert(instr.asmExpr()), null, null);
      }
    } else if (mode instanceof FragmentParser.AsmModeSPIndIdxContext instr) {
      if (instr.ASM_NAME(0).getText().equalsIgnoreCase("sp")
          && instr.ASM_NAME(1).getText().equalsIgnoreCase("y")) {
        return new Fragment.Instruction(
            mnemonic, Fragment.AddressingMode.SPIND_Y, convert(instr.asmExpr()), null, null);
      }
    } else if (mode instanceof FragmentParser.AsmModeImmAndAbsXContext instr) {
      if (instr.ASM_NAME().getText().equalsIgnoreCase("x")) {
        return new Fragment.Instruction(
            mnemonic,
            Fragment.AddressingMode.IMM_ABS_X,
            convert(instr.asmExpr(0)),
            convert(instr.asmExpr(1)),
            null);
      }
    }
    throw new RuntimeException("Addressing mode not supported: " + mode.getText());
  }

  private Fragment.Expr convert(FragmentParser.AsmExprContext ctx) {
    if (ctx instanceof FragmentParser.AsmExprIntContext intCtx) {
      return new Fragment.Expr.Int(Integer.parseInt(intCtx.ASM_NUMBER().getText()));
    } else if (ctx instanceof FragmentParser.AsmExprLabelContext idCtx) {
      return new Fragment.Expr.Identifier(idCtx.ASM_NAME().getText());
    } else if (ctx instanceof FragmentParser.AsmExprBinaryContext binCtx) {
      return new Fragment.Expr.Binary(
          Fragment.Expr.BinOp.fromOp(binCtx.getChild(1).getText()),
          convert(binCtx.asmExpr(0)),
          convert(binCtx.asmExpr(1)));
    } else if (ctx instanceof FragmentParser.AsmExprUnaryContext unCtx) {
      return new Fragment.Expr.Unary(
          Fragment.Expr.UnOp.fromOp(unCtx.getChild(0).getText()), convert(unCtx.asmExpr()));
    } else {
      throw new RuntimeException("Unexpected value: " + ctx.getText());
    }
  }

  private Fragment.Label convert(FragmentParser.AsmLabelContext ctx) {
    final String text = ctx.getText();
    return new Fragment.Label(text.substring(0, text.length() - 1));
  }

  private Fragment.Element convert(FragmentParser.AsmDirectiveContext ctx) {
    if (ctx instanceof FragmentParser.AsmDirectiveCpuContext cpu) {
      return new Fragment.Cpu(cpu.ASM_NAME().getText());
    } else {
      throw new RuntimeException("Unexpected value: " + ctx.getText());
    }
  }
}
