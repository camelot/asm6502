package dk.camelot64.asm6502.fragment;

/** A GraphViz graph with sub-graphs. */
public class GraphViz {

  StringBuilder graph;

  public GraphViz() {
    this.graph = new StringBuilder();
  }

  public String getGraph() {
    return graph.toString();
  }

  public void header() {
    graph.append(
        """
          digraph {
            labeljust="l";
            fontname="Arial";
            edge [fontname="Arial"];
            node [fontname="Arial",fillcolor="lightyellow"];
          """);
  }

  public void footer() {
    graph.append("}\n");
  }

  public void subgraphStart(String name, String title) {
    graph
        .append("  subgraph cluster_")
        .append(name)
        .append(" {\n")
        .append("    label=\"")
        .append(title)
        .append("\"\n");
  }

  public void subgraphEnd() {
    graph.append("  }\n");
  }

  public void edge(
      String fromPrettyId, String toPrettyId, String label, String style, String arrowhead) {
    graph.append("    ").append(fromPrettyId).append(" -> ").append(toPrettyId);
    graph.append("[");
    if (label != null) {
      graph.append("label=\"").append(label).append("\"");
    }
    if (style != null) {
      graph.append(",style=\"" + style + "\"");
    }
    if (arrowhead != null) {
      graph.append(",arrowhead=\"" + arrowhead + "\"");
    }
    graph.append("];\n");
  }

  public void node(String prettyId, String label, boolean filled, String fontName, String shape) {
    graph.append("    ").append(prettyId).append(" [");
    graph.append("label=\"").append(label).append("\"");
    if (filled) {
      graph.append(",style=\"filled\"");
    }
    if (fontName != null) {
      graph.append(",fontname=\"").append(fontName).append("\"");
    }
    if (shape != null) {
      graph.append(",shape=\"").append(shape).append("\"");
    }
    graph.append("];\n");
  }

  public void styles(String styles) {
    graph.append(styles).append("\n");
  }
}
