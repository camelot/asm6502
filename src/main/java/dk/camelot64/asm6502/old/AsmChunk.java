package dk.camelot64.asm6502.old;

import dk.camelot64.asm6502.cpufamily.CpuClobber;
import dk.camelot64.asm6502.cpufamily.CpuOpcode;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * A chunk of an ASM program. The chunk has a number of methods/attributes that describe the lines
 * of the chunk. Typically each ICL statement becomes a single ASM chunk through the AsmFragment
 * subsystem.
 */
public class AsmChunk {

  /** The lines of the chunk. */
  private List<AsmLine> lines;

  /** Index of the chunk. */
  private int index;

  /**
   * If non-null this overwrites the clobber of the chunk that is calculated by examining the ASM
   * instruction lines.
   */
  private CpuClobber clobberOverwrite;

  public AsmChunk(int index) {
    this.lines = new ArrayList<>();
    this.index = index;
  }

  public List<AsmLine> getLines() {
    return lines;
  }

  public void addLine(AsmLine line) {
    lines.add(line);
  }

  public CpuClobber getClobberOverwrite() {
    return clobberOverwrite;
  }

  public void setClobberOverwrite(CpuClobber clobberOverwrite) {
    this.clobberOverwrite = clobberOverwrite;
  }

  /**
   * Add a new line just after another line
   *
   * @param line The line to look for. If it is not found an Exception is thrown
   * @param add The line to add
   */
  public void addLineAfter(AsmLine line, AsmLine add) {
    ListIterator<AsmLine> it = lines.listIterator();
    while (it.hasNext()) {
      AsmLine asmLine = it.next();
      if (line.equals(asmLine)) {
        it.add(add);
        return;
      }
    }
    throw new NoSuchElementException("Item not found " + line);
  }

  public int getIndex() {
    return index;
  }

  /**
   * Get the number of bytes the chunk occupies in memory. Per default calculated by adding up the
   * bytes of each ASM line in the chunk.
   *
   * @return The number of bytes
   */
  public int getBytes() {
    int bytes = 0;
    for (AsmLine line : lines) {
      bytes += line.getLineBytes();
    }
    return bytes;
  }

  /**
   * Get the number of cycles it takes to execute the chunk Per default calculated by adding up the
   * cycles of each ASM line in the chunk.
   *
   * @return The number of cycles
   */
  public double getCycles() {
    double cycles = 0.0;
    for (AsmLine line : lines) {
      cycles += line.getLineCycles();
    }
    return cycles;
  }

  /**
   * Get the registers clobbered when executing the chunk Per default calculated by adding up the
   * clobber of each ASM line in the chunk.
   *
   * @return The registers clobbered
   */
  public CpuClobber getClobber() {
    if (clobberOverwrite != null) {
      return clobberOverwrite;
    }
    CpuClobber chunkClobber = CpuClobber.CLOBBER_NONE;
    for (AsmLine line : lines) {
      if (line instanceof AsmInstruction) {
        AsmInstruction asmInstruction = (AsmInstruction) line;
        CpuOpcode cpuOpcode = asmInstruction.getCpuOpcode();
        CpuClobber opcodeClobber = cpuOpcode.getClobber();
        chunkClobber = new CpuClobber(chunkClobber, opcodeClobber);
      }
    }
    return chunkClobber;
  }

  /**
   * Get ASM line by index
   *
   * @param idx The index of the line to get
   * @return The line with the passed index. Null if not found inside the chunk.
   */
  public AsmLine getAsmLine(int idx) {
    for (AsmLine asmLine : getLines()) {
      if (asmLine.getIndex() == idx) {
        return asmLine;
      }
    }
    return null;
  }

  public String toString(AsmProgram.AsmPrintState printState) {
    StringBuffer out = new StringBuffer();
    for (int i = 0; i < lines.size(); i++) {
      AsmLine line = lines.get(i);
      if (line instanceof AsmScopeEnd) {
        printState.decIndent();
      }
      if (printState.getAsmLineNumber()) {
        out.append("[" + line.getIndex() + "]");
      }
      out.append(printState.getIndent());
      if (shouldIndentAsm(line)) {
        out.append("  ");
      }
      if (line instanceof AsmComment) {
        // Peek forward to find the comment indent
        for (int j = i; j < lines.size(); j++) {
          AsmLine peek = lines.get(j);
          if (peek instanceof AsmScopeBegin) {
            break;
          } else if (shouldIndentAsm(peek)) {
            out.append("  ");
            break;
          }
        }
      }
      out.append(line.getAsm() + "\n");
      if (line instanceof AsmScopeBegin) {
        printState.incIndent();
      }
    }
    return out.toString();
  }

  /**
   * Should this ASM line be indented a few spaces. Instructions, variables and similar ASM is
   * indented slightly more than other ASM.
   *
   * @param line The line to examine
   * @return true if the line is an instruction or similar.
   */
  private boolean shouldIndentAsm(AsmLine line) {
    return line instanceof AsmInstruction
        || line instanceof AsmLabelDecl
        || line instanceof AsmConstant
        || line instanceof AsmDataNumeric
        || line instanceof AsmDataZeroFill
        || line instanceof AsmDataString
        || line instanceof AsmDataAlignment
        || (line instanceof AsmInlineKickAsm && !line.getAsm().contains(".pc"));
  }

  @Override
  public String toString() {
    return toString(new AsmProgram.AsmPrintState(true));
  }
}
