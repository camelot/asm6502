package dk.camelot64.asm6502.old;

import dk.camelot64.asm6502.encoding.CharEncoding;

/** Set the text encoding */
public class AsmSetEncoding extends AsmLine {

  private final CharEncoding encoding;

  public AsmSetEncoding(CharEncoding encoding) {
    this.encoding = encoding;
  }

  @Override
  public int getLineBytes() {
    return 0;
  }

  @Override
  public double getLineCycles() {
    return 0;
  }

  @Override
  public String getAsm() {
    return ".encoding \"" + encoding.asmEncoding + "\"";
  }

  public CharEncoding getEncoding() {
    return encoding;
  }
}
