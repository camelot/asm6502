package dk.camelot64.asm6502.old;

import dk.camelot64.asm6502.cpufamily.Cpu65xx;

/** Set the current CPU */
public class AsmSetCpu extends AsmLine {

  private final Cpu65xx cpu;

  public AsmSetCpu(Cpu65xx cpu) {
    this.cpu = cpu;
  }

  @Override
  public int getLineBytes() {
    return 0;
  }

  @Override
  public double getLineCycles() {
    return 0;
  }

  @Override
  public String getAsm() {
    final String cpuName = cpu.getKickassCpu().name;
    return ".cpu " + cpuName;
  }

  public Cpu65xx getCpu() {
    return cpu;
  }
}
