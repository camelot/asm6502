// ASM grammar
parser grammar FragmentParser;

options { tokenVocab=FragmentLexer; }

asmFile
    : asmLines EOF
    ;

asmLines
    : asmLine*
    ;

asmLine
    : asmDirective
    | asmLabel
    | asmInstruction
    | asmBytes
    ;


asmDirective
    : ASM_CPU ASM_NAME #asmDirectiveCpu
    ;

asmLabel
    : ASM_NAME ASM_COLON ASM_TAG* #asmLabelName
    | ASM_CURLY_BEGIN ASM_NAME ASM_CURLY_END ASM_COLON ASM_TAG* #asmLabelReplace
    | ASM_MULTI_NAME ASM_COLON ASM_TAG* #asmLabelMulti
    ;

asmInstruction
    : ASM_MNEMONIC (asmParamMode)? ASM_TAG*
    ;

asmBytes
    : ASM_BYTE asmExpr ( ASM_COMMA asmExpr)* ASM_TAG*
    ;

asmParamMode
    : asmExpr #asmModeAbs
    | asmExpr ASM_COMMA ASM_NAME #asmModeAbsXY
    | ASM_IMM asmExpr #asmModeImm
    | asmExpr ASM_COMMA asmExpr #asmModeAbs2
    | asmExpr ASM_COMMA asmExpr ASM_COMMA asmExpr #asmModeAbs3
    | ASM_PAR_BEGIN asmExpr ASM_PAR_END ASM_COMMA ASM_NAME #asmModeIndIdxYZ
    | ASM_PAR_BEGIN ASM_PAR_BEGIN asmExpr ASM_PAR_END ASM_PAR_END ASM_COMMA ASM_NAME #asmModeIndLongIdxZ
    | ASM_PAR_BEGIN asmExpr ASM_COMMA ASM_NAME ASM_PAR_END ASM_COMMA ASM_NAME #asmModeSPIndIdx
    | ASM_PAR_BEGIN asmExpr ASM_COMMA ASM_NAME ASM_PAR_END #asmModeIdxIndX
    | ASM_PAR_BEGIN asmExpr ASM_PAR_END  #asmModeInd
    | ASM_PAR_BEGIN ASM_PAR_BEGIN asmExpr ASM_PAR_END ASM_PAR_END  #asmModeIndLong
    | ASM_IMM asmExpr ASM_COMMA asmExpr #asmModeImmAndAbs
    | ASM_IMM asmExpr ASM_COMMA asmExpr ASM_COMMA ASM_NAME #asmModeImmAndAbsX
    ;

asmExpr
    : ASM_BRACKET_BEGIN asmExpr ASM_BRACKET_END #asmExprPar
    | asmExpr ( ASM_DOT ) asmExpr #asmExprBinary
    | asmExpr ( ASM_SHIFT_LEFT| ASM_SHIFT_RIGHT ) asmExpr #asmExprBinary
    | (ASM_PLUS | ASM_MINUS| ASM_LESS_THAN | ASM_GREATER_THAN ) asmExpr #asmExprUnary
    | asmExpr (ASM_MULTIPLY | ASM_DIVIDE ) asmExpr #asmExprBinary
    | asmExpr ( ASM_PLUS | ASM_MINUS )  asmExpr #asmExprBinary
    | ASM_NAME #asmExprLabel
    | ASM_MULTI_REL #asmExprLabelRel
    | ASM_CURLY_BEGIN ASM_NAME ASM_CURLY_END #asmExprReplace
    | ASM_NUMBER #asmExprInt
    | ASM_CHAR #asmExprChar
    ;
