package dk.camelot64.asm6502.fragment;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestStringToFragment {

  @Test
  void testAsmFragmentParser() {
    final Fragment fragment = StringToFragment.parse("lda #1 sta 2");
    Assertions.assertEquals(fragment.body().size(), 2);
    Assertions.assertNotNull(fragment.body().get(0));
    Assertions.assertInstanceOf(Fragment.Instruction.class, fragment.body().get(0));
    final Fragment.Instruction lda = (Fragment.Instruction) fragment.body().get(0);
    Assertions.assertEquals(lda.mnemonic(), "lda");
    Assertions.assertEquals(lda.mode(), Fragment.AddressingMode.IMM);
    Assertions.assertNull(lda.operand2());
    Assertions.assertNull(lda.operand3());
    Assertions.assertNotNull(lda.operand1());
    Assertions.assertInstanceOf(Fragment.Expr.Int.class, lda.operand1());
    Assertions.assertEquals(((Fragment.Expr.Int) lda.operand1()).value(), 1);

    Assertions.assertNotNull(fragment.body().get(1));
    Assertions.assertInstanceOf(Fragment.Instruction.class, fragment.body().get(1));
    final Fragment.Instruction sta = (Fragment.Instruction) fragment.body().get(1);
    Assertions.assertEquals(sta.mnemonic(), "sta");
    Assertions.assertEquals(sta.mode(), Fragment.AddressingMode.ABS);
    Assertions.assertNull(sta.operand2());
    Assertions.assertNull(sta.operand3());
    Assertions.assertNotNull(sta.operand1());
    Assertions.assertInstanceOf(Fragment.Expr.Int.class, sta.operand1());
    Assertions.assertEquals(((Fragment.Expr.Int) sta.operand1()).value(), 2);
  }

  @Test
  void testKickAssTrivial() {
    final String input = "lda #1 sta 2";
    final String expected = """
            lda #1
            sta 2
            """;
    assertParseToKickAss(input, expected);
  }

  @Test
  void testKickAssExpr() {
    final String input = "lda #1+2";
    final String expected = """
            lda #[1+2]
            """;
    assertParseToKickAss(input, expected);
  }

  @Test
  void testKickAssExprPrecesence() {
    final String input = "lda #1+2*-3<<a.b";
    final String expected = """
            lda #[1+[2*[-[3<<[a.b]]]]]
            """;
    assertParseToKickAss(input, expected);
  }

  @Test
  void testKickAssExprPrecesence2() {
    final String input = "lda #a.b>><3/2-1";
    final String expected = """
            lda #[[[[a.b]>>[<3]]/2]-1]
            """;
    assertParseToKickAss(input, expected);
  }

  @Test
  void testKickAssLoop() {
    final String input =
        """
                  ldx #0
                  next:
                  inx
                  cpx #10
                  bcc next
                  """;
    final String expected =
        """
            ldx #0
            next: inx
            cpx #10
            bcc next
            """;
    assertParseToKickAss(input, expected);
  }

  @Test
  void testKickAssAddressingModes() {
    final String input =
        """
                  inx
                  lda #10
                  lda 10
                  lda 10, 10
                  lda 10, 10, 10
                  lda 10,x
                  lda 10,y
                  lda (10), y
                  lda (10), z
                  lda ((10)), z
                  lda (10,sp), y
                  lda (10,x)
                  lda (10)
                  lda ((10))
                  lda #10, 10
                  lda #10, 10,x
                  """;
    final String expected =
        """
                  inx
                  lda #10
                  lda 10
                  lda 10,10
                  lda 10,10,10
                  lda 10,x
                  lda 10,y
                  lda (10),y
                  lda (10),z
                  lda ((10)),z
                  lda (10,sp),y
                  lda (10,x)
                  lda (10)
                  lda ((10))
                  lda #10,10
                  lda #10,10,x
                  """;
    assertParseToKickAss(input, expected);
  }

  private void assertParseToKickAss(String input, String expected) {
    final Fragment asm = StringToFragment.parse(input);
    final String kickAsm = FragmentToKickAss.toAsm(asm);
    Assertions.assertEquals(expected, kickAsm);
  }
}
