package dk.camelot64.asm6502.fragment;

import dk.camelot64.asm6502.fragment.model.Model;
import dk.camelot64.asm6502.fragment.model.ModelPretty;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestFragmentForger {

  @Test
  void testSignatureConverter() {
    // vbuaa=vbuxx
    Signature signature =
        new Signature.Assignment(
            new Signature.Expr.Register(Signature.Expr.Reg.AA, Signature.Type.Basic.U8),
            new Signature.Expr.Register(Signature.Expr.Reg.XX, Signature.Type.Basic.U8));
    final Model asmModel = SignatureToModel.convert(signature, 0);
    Assertions.assertNotNull(asmModel);
    Assertions.assertNotNull(asmModel.places());
    Assertions.assertEquals(2, asmModel.places().size());
    Assertions.assertEquals(
        Model.PlaceId.of(Model.PlaceKind.REGISTER, 1, true), asmModel.places().get(0).id());
    Assertions.assertEquals(
        Model.PlaceId.of(Model.PlaceKind.REGISTER, 2, false), asmModel.places().get(1).id());
    Assertions.assertEquals(
        asmModel.places().get(0),
        asmModel.get(Model.PlaceId.of(Model.PlaceKind.REGISTER, 1, true)));
    Assertions.assertEquals(
        asmModel.places().get(1),
        asmModel.get(Model.PlaceId.of(Model.PlaceKind.REGISTER, 2, false)));
    Assertions.assertNull(asmModel.get(Model.PlaceId.of(Model.PlaceKind.REGISTER, 0, false)));
    Assertions.assertNull(asmModel.get(Model.PlaceId.of(Model.PlaceKind.ZEROPAGE, 1, false)));
    Assertions.assertNull(asmModel.get(Model.PlaceId.of(Model.PlaceKind.REGISTER, 3, false)));
    Assertions.assertInstanceOf(
        Model.Place.Register.class,
        asmModel.get(Model.PlaceId.of(Model.PlaceKind.REGISTER, 1, true)));
    Assertions.assertInstanceOf(
        Model.Place.Register.class,
        asmModel.get(Model.PlaceId.of(Model.PlaceKind.REGISTER, 2, false)));
    Assertions.assertEquals(
        Model.Place.Reg.AA,
        asmModel.get(Model.PlaceId.of(Model.PlaceKind.REGISTER, 1, true)).reg());
    Assertions.assertEquals(
        Model.Place.Reg.XX,
        asmModel.get(Model.PlaceId.of(Model.PlaceKind.REGISTER, 2, false)).reg());
    Assertions.assertNotNull(asmModel.operations());
    Assertions.assertEquals(1, asmModel.operations().size());
    Assertions.assertInstanceOf(Model.Operation.class, asmModel.operations().get(0));
    Assertions.assertEquals(
        Model.PlaceId.of(Model.PlaceKind.REGISTER, 1, true),
        asmModel.operations().get(0).output().id());
    Assertions.assertEquals(
        Model.PlaceId.of(Model.PlaceKind.REGISTER, 2, false),
        asmModel.operations().get(0).input().id());
    // The lists are unmodifiable
    Assertions.assertThrows(UnsupportedOperationException.class, () -> asmModel.places().add(null));
    Assertions.assertThrows(
        UnsupportedOperationException.class, () -> asmModel.operations().add(null));
  }

  @Test
  void testFragmentForger() {
    // vbuaa=vbuxx
    Signature signature =
        new Signature.Assignment(
            new Signature.Expr.Register(Signature.Expr.Reg.AA, Signature.Type.Basic.U8),
            new Signature.Expr.Register(Signature.Expr.Reg.XX, Signature.Type.Basic.U8));

    Fragment fragment = Forger.forge(signature, ForgerDebug.NONE);
    Assertions.assertNotNull(fragment);
    Assertions.assertNotNull(fragment.body());
    Assertions.assertEquals(1, fragment.body().size());
    Assertions.assertInstanceOf(Fragment.Instruction.class, fragment.body().get(0));
    Assertions.assertEquals("txa", ((Fragment.Instruction) fragment.body().get(0)).mnemonic());
  }

  private void assertSigToAsm(String sig, String asm) {
    final String kickAsm = sigToAsm(sig);
    Assertions.assertEquals(asm, kickAsm);
  }

  private static class ForgerGraphAndPretty implements ForgerDebug {

    ForgerGraph graph;

    public ForgerGraphAndPretty() {
      this.graph = new ForgerGraph();
    }

    @Override
    public void signature(Signature signature) {
      graph.signature(signature);
    }

    @Override
    public void model(Model model, String roundTitle, Model diff) {
      System.out.println("" + model.id() + ": " + roundTitle);
      System.out.println(ModelPretty.pretty(model));
      graph.model(model, roundTitle, diff);
    }

    @Override
    public void asm(Fragment fragment) {
      graph.asm(fragment);
    }

    public String getGraph() {
      return graph.getGraph();
    }
  }

  private String sigToAsm(String sig) {
    Signature signature = StringToSignature.parse(sig);
    final ForgerGraphAndPretty graphAndPretty = new ForgerGraphAndPretty();
    Fragment fragment;
    try {
      fragment = Forger.forge(signature, graphAndPretty);
    } catch (RuntimeException e) {
      System.out.println("-------------------------");
      System.out.println(graphAndPretty.getGraph());
      System.out.println("-------------------------");
      throw e;
    }
    System.out.println("-------------------------");
    System.out.println(graphAndPretty.getGraph());
    return FragmentToKickAss.toAsm(fragment);
  }

  @Test
  void testTxa() {
    assertSigToAsm("vbuaa=vbuxx", "txa\n");
  }

  @Test
  void testStx() {
    assertSigToAsm("vbum1=vbuxx", "stx {m1}\n");
  }

  @Test
  void testStaMem() {
    assertSigToAsm("vbum1=vbuaa", "sta {m1}\n");
  }

  @Test
  void testLdaMem() {
    assertSigToAsm("vbuaa=vbum1", "lda {m1}\n");
  }

  @Test
  void testLdaConst() {
    assertSigToAsm("vbuaa=vbuc1", "lda #{c1}\n");
  }

  @Test
  void testMemMov() {
    assertSigToAsm("vbum1=vbum2", "lda {m2}\nsta {m1}\n");
  }

  @Test
  void testLdaIndZpY() {
    assertSigToAsm("vbuaa=pbuz1[vbuyy]", "lda ({z1}),y\n");
  }

  @Test
  void testLdaIndZpYFromZp() {
    assertSigToAsm("vbuaa=pbuz1[vbuz2]", "ldy {z2}\nlda ({z1}),y\n");
  }

  @Test
  void testStaIndZpY() {
    assertSigToAsm("pbuz1[vbuyy]=vbuaa", "sta ({z1}),y\n");
  }

  @Test
  void testLdaStaIndZpY() {
    assertSigToAsm("\"pbuz1[vbuyy]=pbuz2[vbuyy]\"", "lda ({z2}),y\nsta ({z1}),y\n");
  }

  @Test
  void testStxIndZpY() {
    assertSigToAsm("pbuz1[vbuyy]=vbuxx", "txa\nsta ({z1}),y\n");
  }

  @Test
  void testStaAbs() {
    assertSigToAsm("*pbuc1=vbuaa", "sta {c1}\n");
  }

  @Test
  void testConstToConstPtr() {
    assertSigToAsm("*pbuc1=vbuc2", "lda #{c2}\nsta {c1}\n");
  }

  @Test
  void testStxAbs() {
    assertSigToAsm("*pbuc1=vbuxx", "stx {c1}\n");
  }

  @Test
  void testZpToConstPtr() {
    assertSigToAsm("*pbuc1=vbuz2", "lda {z2}\nsta {c1}\n");
  }

  @Test
  void testLdaStaZpDerefIdxZp() {
    assertSigToAsm("pbuz1[vbuz2]=pbuz3[vbuz4]", "ldy {z4}\nlda ({z3}),y\nldy {z2}\nsta ({z1}),y\n");
  }

  @Test
  void testDerefConstToZpY() {
    assertSigToAsm("pbuz1[vbuz2]=*pbuc3", "ldy {z2}\nlda {c3}\nsta ({z1}),y\n");
  }

  @Test
  void testConstToZpY() {
    assertSigToAsm("pbuz1[vbuz2]=vbuc3", "ldy {z2}\nlda #{c3}\nsta ({z1}),y\n");
  }

  @Test
  void testConstToZpDerefConst() {
    assertSigToAsm("pbuz1[vbuc2]=vbuc3", "ldy #{c2}\nlda #{c3}\nsta ({z1}),y\n");
  }

  @Test
  void testStoreConstPtrIndexed() {
    assertSigToAsm("pbuc1[vbuxx]=vbuaa", "sta {c1},x\n");
  }

  @Test
  void testLoadConstPtrIndexed() {
    assertSigToAsm("vbuaa=pbuc1[vbuyy]", "lda {c1},y\n");
  }

  @Test
  void testDerefIzYToAbsYy() {
    assertSigToAsm("pbuz1[vbuyy]=pbuc2[vbuyy]", "lda {c2},y\nsta ({z1}),y\n");
  }

  @Test
  void testMultiDeref() {
    assertSigToAsm(
        "pbuz1[pbuz2[vbuz3]]=vbuz4", "ldy {z3}\nlda ({z2}),y\ntay\nlda {z4}\nsta ({z1}),y\n");
  }

  @Test
  void testMultiDeref2() {
    // TODO: pbuz2[vbuyy] should be stored to tmp0 using sta instead of tay, sta tmp0
    assertSigToAsm(
        "pbuz1[pbuz2[vbuyy]]=pbuz4[vbuz6]",
        "lda ({z2}),y\ntay\nsty tmp0\nldy {z6}\nlda ({z4}),y\nldy tmp0\nsta ({z1}),y\n");
  }

  @Test
  void testCommonNaming() {
    assertSigToAsm("vbuz1=vbuc1", "lda #{c1}\nsta {z1}\n");
  }

  @Test
  void testLdaAbsIdxZp() {
    assertSigToAsm("vbuaa=pbuc1[vbuz1]", "ldx {z1}\nlda {c1},x\n");
  }

  @Test
  void testAbsIdxZpFromZpIdxZp() {
    assertSigToAsm("pbuc1[vbuz1]=pbuz2[vbuz3]", "ldx {z1}\nldy {z3}\nlda ({z2}),y\nsta {c1},x\n");
  }

  @Test
  void testCombineLeafsYy() {
    assertSigToAsm("pbuc1[vbuyy]=vbuyy", "tya\nsta {c1},y\n");
  }

  @Test
  void testCombineLeafsZ1() {
    assertSigToAsm("pbuc1[vbuz1]=pbuz2[vbuz1]", "ldy {z1}\nlda ({z2}),y\nsta {c1},y\n");
  }

  @Test
  void testCombineLeafsZ1b() {
    assertSigToAsm("pbuz1[vbuz2]=pbuz3[vbuz2]", "ldy {z2}\nlda ({z3}),y\nsta ({z1}),y\n");
  }

  @Test
  void testCombineLeafsZ1c() {
    assertSigToAsm("vbuaa=pbuz3[vbuz2]_bor_pbuz1[vbuz2]", "ldy {z2}\nlda ({z3}),y\nora ({z1}),y\n");
  }

  @Test
  void testCombineLeafsZ1d() {
    assertSigToAsm(
        "pbuz4[vbuz2]=pbuz3[vbuz2]_bor_pbuz1[vbuz2]",
        "ldy {z2}\nlda ({z3}),y\nora ({z1}),y\nsta ({z4}),y\n");
  }

  @Test
  void testBinSimpleTypes() {
    assertSigToAsm(
        "vbuaa=vbuc1_bor_pbuz1[vbuyy]_bor_pbuc1[vbuxx]_bor_vbuz2_bor_vbum3",
        "lda #{c1}\nora ({z1}),y\nora {c1},x\nora {z2}\nora {m3}\n");
  }

  @Test
  void testSimpleOr() {
    assertSigToAsm("vbuaa=vbuaa_bor_vbuz1", "ora {z1}\n");
  }

  @Test
  void testSimpleAnd() {
    assertSigToAsm("vbuaa=vbuaa_band_vbuz1", "and {z1}\n");
  }

  @Test
  void testSimpleXor() {
    assertSigToAsm("vbuaa=vbuaa_bxor_vbuz1", "eor {z1}\n");
  }

  @Test
  void testSimpleOrToZp1() {
    assertSigToAsm("vbuz1=vbuaa_bor_vbuz1", "ora {z1}\nsta {z1}\n");
  }

  @Test
  void testSimpleOrToZp() {
    assertSigToAsm("vbuz1=vbuaa_bor_vbuz2", "ora {z2}\nsta {z1}\n");
  }

  @Test
  void testSimpleOrTwoZp() {
    assertSigToAsm("vbuaa=vbuz1_bor_vbuz2", "lda {z1}\nora {z2}\n");
  }

  @Test
  void testOrToDerefZpY() {
    assertSigToAsm("pbuz1[vbuz2]=vbuaa_bor_vbuz3", "ora {z3}\nldy {z2}\nsta ({z1}),y\n");
  }

  @Test
  void testOrDerefZpYToDerefZpY() {
    assertSigToAsm("pbuz1[vbuz2]=vbuaa_bor_pbuz1[vbuz2]", "ldy {z2}\nora ({z1}),y\nsta ({z1}),y\n");
  }

  @Test
  void testOrAbsXToAbsX() {
    assertSigToAsm("pbuc1[vbuz1]=vbuaa_bor_pbuc1[vbuz1]", "ldx {z1}\nora {c1},x\nsta {c1},x\n");
  }

  @Test
  void testOrAbsXToDerefZpY() {
    assertSigToAsm(
        "pbuz1[vbuz2]=vbuaa_bor_pbuc1[vbuz3]", "ldy {z2}\nldx {z3}\nora {c1},x\nsta ({z1}),y\n");
  }

  @Test
  void testOrDouble() {
    assertSigToAsm(
        "vbuaa=(vbuaa_bor_pbuc1[vbuz3])_bor_pbuc2[vbuz3]", "ldx {z3}\nora {c1},x\nora {c2},x\n");
  }

  @Test
  void testOrTriple() {
    assertSigToAsm(
        "vbuaa=vbuaa_bor_pbuc1[vbuz3]_bor_pbuc2[vbuz3]_bor_pbuc3[vbuz3]",
        "ldx {z3}\nora {c1},x\nora {c2},x\nora {c3},x\n");
  }

  @Test
  void testOrDoubleOraSame() {
    assertSigToAsm(
        "vbuaa=(vbuaa_bor_pbuc1[vbuz3])_bor_pbuc1[vbuz3]", "ldx {z3}\nora {c1},x\nora {c1},x\n");
  }

  @Test
  void testOrSimpleOra() {
    assertSigToAsm("vbuaa=vbuaa_bor_pbuc1[vbuz3]", "ldx {z3}\nora {c1},x\n");
  }

  @Test
  void testOrDerefZpYToAbsX() {
    assertSigToAsm(
        "pbuc1[vbuz1]=vbuaa_bor_pbuz2[vbuz3]", "ldx {z1}\nldy {z3}\nora ({z2}),y\nsta {c1},x\n");
  }

  @Test
  void testOrAbsYToDerefZpY() {
    assertSigToAsm("pbuz1[vbuz2]=vbuaa_bor_pbuc1[vbuz2]", "ldy {z2}\nora {c1},y\nsta ({z1}),y\n");
  }

  @Test
  void testMultipleComplexDerefs() {
    assertSigToAsm(
        "pbuz1[vbuz4]=pbuz3[vbuz4]_bor_pbuc1[vbuz2]",
        "ldy {z4}\nldx {z2}\nlda ({z3}),y\nora {c1},x\nsta ({z1}),y\n");
  }

  @Test
  void testMultipleComplexDerefs2() {
    assertSigToAsm(
        "pbuz1[vbuz4]=pbuz3[vbuz4]_bor_pbuz1[vbuz2]",
        "ldy {z2}\nlda ({z1}),y\nldy {z4}\nora ({z3}),y\nsta ({z1}),y\n");
  }

  @Test
  void testMultipleComplexDerefs3() {
    // TODO: Instead of storing and loading to tmp0 Y should be reloaded from {z4}
    assertSigToAsm(
        "pbuz1[vbuz4]=(vbuaa_bor_pbuz3[vbuz4])_band_pbuz2[vbuz5]",
        "ldy {z4}\nora ({z3}),y\nsty tmp0\nldy {z5}\nand ({z2}),y\nldy tmp0\nsta ({z1}),y\n");
  }

  /* TODO: move inputs through tmp storage if they are in an unsupported register
  @Test
  void testYorA() {
    assertSigToAsm(
          "vbuaa=vbuaa_bor_vbuyy",
          "");
  }
   */

  @Test
  void testMultiIndirection() {
    assertSigToAsm(
        "pbuz1[pbuz2[vbuz3]]=vbuz4", "ldy {z3}\nlda ({z2}),y\ntay\nlda {z4}\nsta ({z1}),y\n");
  }

  @Test
  void testMultiIndirection2() {
    assertSigToAsm(
        "pbuz1[pbuz2[vbuz3]]=pbuz4[pbuz2[vbuz3]]",
        "ldy {z3}\nlda ({z2}),y\ntay\nlda ({z4}),y\nsta ({z1}),y\n");
  }

  @Test
  void testRegsLvalueRvalue() {
    assertSigToAsm("vbuyy=pbuz1[vbuyy]", "lda ({z1}),y\ntay\n");
  }

  @Test
  void testStxAbsX() {
    assertSigToAsm("pbuc1[vbuxx]=vbuxx", "txa\nsta {c1},x\n");
  }

  @Test
  void testLdyAbsY() {
    // TODO: LDY abs,Y is not supported. Move through AA.
    assertSigToAsm("vbuyy=pbuc1[vbuyy]", "ldy {c1},y\n");
  }

  @Test
  void testIny() {
    assertSigToAsm("vbuyy=inc_vbuyy", "iny\n");
  }

  @Test
  void testDex() {
    assertSigToAsm("vbuxx=dec_vbuxx", "dex\n");
  }

  @Test
  void testMovU16ZtoZ() {
    assertSigToAsm("vwuz1=vwuz2", "lda [{z2}+0]\nsta [{z1}+0]\nlda [{z2}+1]\nsta [{z1}+1]\n");
  }

  @Test
  void testMovU16MtoM() {
    assertSigToAsm("vwum1=vwum2", "lda [{m2}+0]\nsta [{m1}+0]\nlda [{m2}+1]\nsta [{m1}+1]\n");
  }

  @Test
  void testMovU16AbsXtoZ() {
    assertSigToAsm(
        "vwuz1=pwuc1_derefidx_vbuxx",
        "lda [{c1}+0],x\nsta [{z1}+0]\nlda [{c1}+1],x\nsta [{z1}+1]\n");
  }

  @Test
  void testMovU16CtoZ() {
    assertSigToAsm("vwuz1=vwuc1", "lda #[<{c1}]\nsta [{z1}+0]\nlda #[>{c1}]\nsta [{z1}+1]\n");
  }

  @Test
  void testMovU16CtoAbsX() {
    assertSigToAsm(
        "pwuc1[vbuz1]=vwuc2",
        "ldx {z1}\nlda #[<{c2}]\nsta [{c1}+0],x\nlda #[>{c2}]\nsta [{c1}+1],x\n");
  }

  @Test
  void testMovU16ZpYtoMem() {
    assertSigToAsm(
        "vwum1=pwuz1_derefidx_vbuyy",
        "lda ({z1}),y\niny\nsta [{m1}+0]\nlda ({z1}),y\nsta [{m1}+1]\n");
  }

  @Test
  void testMovU16ConsttoZpy() {
    assertSigToAsm(
        "pwuz1_derefidx_vbuyy=vwuc1",
        "lda #[<{c1}]\nsta ({z1}),y\niny\nlda #[>{c1}]\nsta ({z1}),y\n");
  }

  @Test
  void testMovU16ZpYtoZpy() {
    assertSigToAsm(
        "pwuz1_derefidx_vbuyy=pwuz2_derefidx_vbuyy",
        "lda ({z2}),y\nsta ({z1}),y\niny\nlda ({z2}),y\nsta ({z1}),y\n");
  }

  @Test
  void testMovU16AbsYtoZpy() {
    assertSigToAsm(
        "pwuz1_derefidx_vbuyy=pwuc1_derefidx_vbuyy",
        "lda [{c1}+0],y\nsta ({z1}),y\nlda [{c1}+1],y\niny\nsta ({z1}),y\n");
  }

  @Test
  void testU16Xor() {
    assertSigToAsm(
        "vwuz2=vwuz1_bxor_vwuz2",
        "lda [{z1}+0]\neor [{z2}+0]\nsta [{z2}+0]\nlda [{z1}+1]\neor [{z2}+1]\nsta [{z2}+1]\n");
  }

  @Test
  void testU16XorAbsXy() {
    assertSigToAsm(
        "pwuc1_derefidx_vbuxx=vwuz2_bxor_pwuc1_derefidx_vbuyy",
        "lda [{z2}+0]\n"
            + "eor [{c1}+0],y\n"
            + "sta [{c1}+0],x\n"
            + "lda [{z2}+1]\n"
            + "eor [{c1}+1],y\n"
            + "sta [{c1}+1],x\n");
  }

  @Test
  void testU16XorZpY() {
    assertSigToAsm(
        "pwuz1_derefidx_vbuyy=pwuc1_derefidx_vbuyy_bxor_pwuz2_derefidx_vbuyy",
        "lda [{c1}+0],y\n"
            + "eor ({z2}),y\n"
            + "sta ({z1}),y\n"
            + "lda [{c1}+1],y\n"
            + "iny\n"
            + "eor ({z2}),y\n"
            + "sta ({z1}),y\n");
  }

  @Test
  void testU16YyBindingProblem() {
    assertSigToAsm(
        "pwuz1_derefidx_vbuz3=pwuc1_derefidx_vbuyy_bxor_pwuz2_derefidx_vbuz3",
        "lda [{c1}+1],y\n"
            + "sta tmp0\n"
            + "lda [{c1}+0],y\n"
            + "ldy {z3}\n"
            + "eor ({z2}),y\n"
            + "sta ({z1}),y\n"
            + "iny\n"
            + "lda tmp0\n"
            + "eor ({z2}),y\n"
            + "sta ({z1}),y\n");
  }

  @Test
  void testU16YyBindingProblem2() {
    assertSigToAsm(
        "pwuz1_derefidx_vbuyy=pwuc1_derefidx_vbuyy_bxor_pwuz2_derefidx_vbuyy",
        "lda [{c1}+0],y\n"
            + "eor ({z2}),y\n"
            + "sta ({z1}),y\n"
            + "lda [{c1}+1],y\n"
            + "iny\n"
            + "eor ({z2}),y\n"
            + "sta ({z1}),y\n");
  }
}
