package dk.camelot64.asm6502.fragment;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSignatureParser {

  @Test
  void signatureParse() {
    final Signature signature = StringToSignature.parse("vbuz1=vbuaa");
    Assertions.assertNotNull(signature);
    Assertions.assertInstanceOf(Signature.Assignment.class, signature);
    Signature.Assignment assignment = (Signature.Assignment) signature;
    Assertions.assertInstanceOf(Signature.Expr.ZeroPage.class, assignment.lVal());
    Signature.Expr.ZeroPage lVal = (Signature.Expr.ZeroPage) assignment.lVal();
    Assertions.assertEquals(1, lVal.id());
    Assertions.assertEquals(Signature.Type.Basic.U8, lVal.type());
    Assertions.assertInstanceOf(Signature.Expr.Register.class, assignment.rVal());
    Signature.Expr.Register rVal = (Signature.Expr.Register) assignment.rVal();
    Assertions.assertEquals(Signature.Type.Basic.U8, rVal.type());
    Assertions.assertEquals(Signature.Expr.Reg.AA, rVal.reg());
  }

  @Test
  void testSignatures() {
    roundtrip("vbuz1=vbuaa");
    roundtrip("vbuz1=vbum2");
    roundtrip("vbuz1=pbuz1[vbuyy]");
    roundtrip("pbuz1[vbuyy]=pbuz2[vbuz3]");
    roundtrip("pwsm1=pwsc1");
    roundtrip("vbuaa=*pbuz1");
  }

  private void roundtrip(String signatureStringIn) {
    final Signature signature = StringToSignature.parse(signatureStringIn);
    final String signatureStringOut = SignatureToString.toString(signature);
    Assertions.assertEquals(signatureStringIn, signatureStringOut);
  }
}
